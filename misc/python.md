One of these things is not like the others…
===========================================

Python
------

-   [Extensive Python notes](https://chrisalbon.com/)

-   [Dplyr versus
    Pandas](https://jarvmiller.github.io/2018/02/28/r-pandas/)

-   [Dplyr versus
    Python](https://gist.github.com/conormm/fd8b1980c28dd21cfaf6975c86c74d07)
