REGEX = Regular exoressions
===========================

#### Case sensitivity:

Options:  
+ Either take lower and upper in your regex + Transfer the vector to
lowercase: `str_extract(tolower(vector), pattern= "_____")` + Add
ignore\_case to the pattern:
`str_extract(vector, pattern= regex("_____", ignore_case=TRUE))`

#### Generals:

-   `.`: any character except newline
-   `|`: or character
-   `^`: start of the string
-   `$`: end of the string

#### Sequences: (from the RStudio cheatsheet)

![](images/regex.PNG)

Extra notes: + `[:alpha:]` is safer to use than \[a-zA-Z\] because it
takes special characters into account (e.g., é, è, ä, etc.) + In R, need
to use `[[:digit:]]` or it will interpret it as a character class
identical to `[digt:]` + To negate a self-built character class, use
`[^abc]`. To negate the built-in character classes: `[^[:digit:]]` +
Alternative for word boundaries: `\\<` for start of a word and `\>` for
end of a word + If a larger set has to be escaped, use `fixed()`. For
escaped sequences, you can use `writeLines()` to see how R views your
string after all special characters have been parsed

<br>

#### Quantifiers

-   `?`: optional, zero or one
-   `*`: zero or more
-   `+`: one or more
-   `{n}`: exactly n times
-   `{n,}`: n or more times
-   `{n,m}`: between n and m times

To quantify a single character: just add afterwards  
To quantify a group of characters: add between brackets ()

Note: R applies *greedy quantification* and tries to match the longest
sequence possible To revert and have the shortest sequence possible, add
`?` after the quantifier (e.g., `pattern="A.+?A"`)

<br>

#### Back referencing

To match an earlier sequence use backreferencing + Wrap the first
pattern in brackets () + Refer to that pattern later with `\\1`, `\\2`
etc

    # a word, then anything else, then a repeat of that word
    stringr::str_extract(vector, pattern="([[:alpha:]]+).+\\1")

<br>
<hr>
Resources
---------

-   [Rstudio string cheat
    sheet](https://www.rstudio.com/resources/cheatsheets/)

-   [Automated Data collection with R](http://www.r-datacollection.com/)

-   [Suzan Baert Resource
    Repo](https://github.com/suzanbaert/Resources_and_Bookmarks)
