Useful links
============

Webinars
--------

-   [Rstudio webinars and conference
    materials](https://www.rstudio.com/resources/webinars/)

<br>
<hr>
R-News:
-------

-   [R-Weekly](https://rweekly.org/)

-   [rOpenSci](https://news.ropensci.org/)

-   [R Mailing Lists](https://www.r-project.org/mail.html)

-   [R-bloggers](https://www.r-bloggers.com/)

<br>
<hr>
Events:
-------

-   [R Community Events](https://community.rstudio.com/c/irl)

-   [Conferences and
    meetings](https://jumpingrivers.github.io/meetingsR/)

<br>
<hr>
Data Viz:
---------

-   [ggplot2 extensions](https://www.ggplot2-exts.org/)

-   [Graphics principles
    cheatsheet](https://graphicsprinciples.github.io/)

-   [Data Visualization Labs for CS631 at Oregon Health & Science
    University](https://apreshill.github.io/data-vis-labs-2018/)

Blog posts
----------

### Plots

-   [R graph gallery](https://www.r-graph-gallery.com/)

-   [Top 50 ggplot2
    visualizations](http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html)

-   [Compilation of ggplot color
    palettes](https://github.com/EmilHvitfeldt/r-color-palettes)

-   [The ggplot extension gallery](http://www.ggplot2-exts.org/gallery/)

-   [Visual
    vocabulary](https://github.com/ft-interactive/chart-doctor/tree/master/visual-vocabulary)

-   [Highlight
    plots](https://yutani.rbind.io/post/2017-10-06-gghighlight/)

-   [Highlight plots
    v2](https://drsimonj.svbtle.com/plotting-background-data-for-groups-with-ggplot2)

-   [Geofacet plots](http://ryanhafen.com/blog/geofacet)

-   [Take a sad plot and make it
    better](https://github.com/apreshill/ohsu-biodatavis)

-   [Annotations](https://rud.is/b/2016/03/16/supreme-annotations/)

-   [Causal
    Diagram](https://vilmaromero.github.io/post/2017-02-14-causal-diagram/)

-   [A compendium of clean graphs in
    R](http://shinyapps.org/apps/RGraphCompendium/index.php)

-   [4 tools to pick chart
    colors](https://flowingdata.com/2017/10/24/4-tools-to-pick-your-chart-colors/)

### Spatial

-   [Drawing beautiful maps programmatically with R, sf, and
    ggplot2](https://www.r-spatial.org//r/2018/10/25/ggplot2-sf.html)

<br>

### Tidyverse

-   [The tidy tools
    manifesto](https://cran.r-project.org/web/packages/tidyverse/vignettes/manifesto.html)

#### Dplyr advanced

-   [Programming with
    dplyr](http://dplyr.tidyverse.org/articles/programming.html)

-   [Dplyr Data Wrangling (links to part 1
    of 4)](https://suzan.rbind.io/2018/01/dplyr-tutorial-1/)

-   [A data.table and dplyr
    tour](https://atrebas.github.io/post/2019-03-03-datatable-dplyr/)

#### Purrr

-   [Purrr tutorial - Jenny
    Bryan](https://jennybc.github.io/purrr-tutorial/)

-   [Purrr Intro - Emorie
    Beck](https://emoriebeck.github.io/R-tutorials/)

-   [Purrr tutorial - Charlotte
    Wickham](https://github.com/cwickham/purrr-tutorial)

-   [RLadies purrr intro - Jennifer
    Thompson](https://github.com/jenniferthompson/RLadiesIntroToPurrr)

-   [Crazy little thing called purrr (links to part 1
    of 6)](http://colinfay.me/purrr-web-mining/)

-   [Intro to purrr](https://emoriebeck.github.io/R-tutorials/purrr/)

-   [Functional programming with
    purrr](https://towardsdatascience.com/functional-programming-in-r-with-purrr-469e597d0229)

-   [Going Off the Map: Exploring purrr’s Other
    Functions](https://hookedondata.org/going-off-the-map/)

<br>

### Making packages

-   [How To make good packages - Maëlle
    Salmon](https://masalmon.eu/2017/12/11/goodrpackages/)

-   [Making packages from scratch - Hillary
    Parker](https://hilaryparker.com/2014/04/29/writing-an-r-package-from-scratch/)

-   [Package good
    practices](http://mangothecat.github.io/goodpractice/index.html)

-   [Making your first R
    package](http://tinyheero.github.io/jekyll/update/2015/07/26/making-your-first-R-package.html)

-   [R package primer](http://kbroman.org/pkg_primer/)

-   [rOpenSci packages guide](https://ropensci.github.io/dev_guide/)

-   [Writing a package from
    scratch](https://r-mageddon.netlify.com/post/writing-an-r-package-from-scratch/)

<br>

### Slides

-   [R-Ladies presentation
    ninja](https://alison.rbind.io/post/r-ladies-slides/)

-   [Getting started with
    xaringan](https://aczane.netlify.com/2018/03/02/i-m-not-a-ninja-but-i-ve-got-some-slides/)

-   [Xaringan wiki](https://github.com/yihui/xaringan/wiki)

-   [Contribute to OS with Pretty
    Slides](http://www.datalorax.com/talks/cascadia18/#1)

<br>

### Git

-   [Atlassian GIT
    tutorials](https://www.atlassian.com/git/tutorials/git-stash)

<br>

### Webscraping

-   [Scraping Javascript rendered
    sites](https://www.r-bloggers.com/web-scraping-javascript-rendered-sites/)

-   [Scraping Javascript websites in
    R](http://blog.brooke.science/posts/scraping-javascript-websites-in-r/)

<br>

### Downverse

#### R Markdown

-   [R Markdown
    benefits](https://rfortherestofus.com/2019/03/r-killer-feature-rmarkdown/)

-   [R Markdown for writing reproducivle scientific
    reports](https://libscie.github.io/rmarkdown-workshop/handout.html)

-   [Writing your thesis with R
    Markdown](https://rosannavanhespenresearch.wordpress.com/2016/02/03/writing-your-thesis-with-r-markdown-1-getting-started/)

-   [APA documents with R Markdown and
    papaja](https://rpubs.com/YaRrr/papaja_guide)

-   [YAML header
    options](https://rmarkdown.rstudio.com/html_document_format.html)

-   [Markdown cheat
    sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

-   [Pimp my Rmd: A few tips for R
    Markdown](https://www.r-graph-gallery.com/pimp-my-rmd-a-few-tips-for-r-markdown/)

-   [Getting your table of contents where you want
    it](https://www.garrickadenbuie.com/blog/2018/02/28/add-a-generated-table-of-contents-anywhere-in-rmarkdown/)

-   [Automating summary of surveys with
    Rmd](https://rviews.rstudio.com/2017/11/07/automating-summary-of-surveys-with-rmarkdown/)

-   [Radix for R Markdown](https://rstudio.github.io/radix/)

#### Blogdown

-   [Hugo shortcodes](https://github.com/rstudio/blogdown/issues/40)

<br>

### Miscellaneous

-   [library()
    vs. require()](https://yihui.name/en/2014/07/library-vs-require/)

-   [Introducing
    ‘R-Miss-Tastic’](https://www.njtierney.com/post/2019/01/29/intro-r-miss-tastic/)

-   [rstudio::conf 2019 Workshop
    Materials](https://blog.rstudio.com/2019/02/06/rstudio-conf-2019-workshops/)

-   [R tips and
    tricks](https://paulvanderlaken.com/2018/05/21/r-tips-and-tricks/)

-   [Making GIFs in
    R](https://blogdown-demo.rbind.io/2018/01/31/gif-animations/)

-   [“Here” package](https://github.com/jennybc/here_here)

-   [Generating codebooks about
    dataset](http://sandsynligvis.dk/articles/18/codebook.html)

-   [So you’ve been asked to make a
    reprex](https://www.jessemaegan.com/post/so-you-ve-been-asked-to-make-a-reprex/)

-   [Mike Kearney Rtweet
    workshop](https://mkearney.github.io/nicar_tworkshop/#39)

-   [gganimate
    cookbook](https://www.data-imaginist.com/slides/rstudioconf2019/assets/player/keynotedhtmlplayer#0)

-   [Nice color palette](https://github.com/jkaupp/nord)

![](https://github.com/jkaupp/nord/blob/master/man/figures/README-palettes-1.png?raw=true)
