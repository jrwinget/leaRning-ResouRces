Useful links
============

### Starter Guides

-   [Getting started with
    R](https://rfortherestofus.com/courses/getting-started/)

-   [R for data science by Hadley Wickham and Garrett
    Grolemund](http://r4ds.had.co.nz/)

-   [Modern Dive, by Chester Ismay and Albert Y.
    Kim](http://moderndive.com/)

-   [Data Analysis and Visualization Using R, by David
    Robinson](http://varianceexplained.org/RData/)

-   [Learning statistics with R, by Danielle
    Navarro](http://compcogscisydney.org/learning-statistics-with-r/)

-   [A language, not a letter: Learning statistics in
    R](https://ademos.people.uic.edu/)

<br>

### Visualization

-   [R Graphics Cookbook](https://r-graphics.org/)

-   [Cookbook for R: Graphds](http://www.cookbook-r.com/Graphs/)

-   [Tufte in R](http://motioninsocial.com/tufte/)

-   [Data Visualization, a practical introduction, by Kieran
    Healy](http://socviz.co/)

-   [Fundamentals of Data Visualization, by Claus O. Wilke (in
    progress)](http://serialmentor.com/dataviz/)

<br>

### Text Analytics

-   Text Analytics: [Tidy Text Mining, by Julia Silge and David
    Robinson](https://www.tidytextmining.com/)

<br>

### Twitter

-   Twitter data: [21 Recipes for Mining Twitter Data with
    rtweet](https://rud.is/books/21-recipes/)

<br>

Tidyverse
---------

-   [The official guide to the
    tidyverse](https://www.tidyverse.org/packages/)

-   [Tidyverse style guide](https://style.tidyverse.org/)

### Advanced R

-   [R packages, by Hadley Wickham](http://r-pkgs.had.co.nz/)

-   [Advanced R, by Hadley Wickham](http://adv-r.had.co.nz/)

-   [Advanced R solutions](https://advanced-r-solutions.rbind.io/)

-   [Advanced R course](https://privefl.github.io/advr38book/)

<br>

### GIT

-   [Happy with GIT for R, by Jenny Bryan](http://happygitwithr.com/)

-   [Git in practice, by Mike
    McQuaid](https://github.com/GitInPractice/GitInPractice#readme)

<br>

### Downverse

-   [R markdown](https://bookdown.org/yihui/rmarkdown/)

-   Blogdown: [Creating Websites with R Markdown, by Yihui Xie et
    al.](https://bookdown.org/yihui/blogdown/)

-   Bookdown: [Authoring Books and Technical Documents with R Markdown,
    by Yihui Xie](https://bookdown.org/yihui/bookdown/)

-   Pagedown: [Create Paged HTML Documents for Printing from R
    Markdown](https://rstudio.github.io/pagedown/)

-   [papaja: Reproducible APA
    manuscripts](https://crsh.github.io/papaja_man/introduction.html)

<br>

### Others

-   SQL: [Data.World Basic to Advanced
    tutorials](https://docs.data.world/documentation/sql/concepts/basic/intro.html)

-   Feature Engineering: [Feature Engineering and Selection: A Practical
    Approach for Predictive Models](https://bookdown.org/max/FES/)
