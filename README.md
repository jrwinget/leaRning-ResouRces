# leaRning-ResouRces Repo

This repo contains some of the summaries/notes/snippets of R code I've found useful over the years. 
It also contains bookmarks of helpful blog posts and tutorials.

I created this repo mostly for myself and my students, but I thought others might benefit from these materials as well.

All docs are subject to change and are periodically updated.

<br><hr>

## Links

+ [Books](links/books.md)

+ [Bookmarks](links/bookmarks.md)

<br><hr>

## Code snippets

+ [dplyr snippets](snippets/dplyr.md)

+ [Categorical data (forcats)](snippets/categorical-data.md)

+ [Experimental designs](snippets/exp-design.md)

+ [Function recepie](snippets/functions-recepie.md)

+ [Templates for writing functions](snippets/functions.md)

+ [Text analysis](snippets/text-analysis.md)

+ [Sentiment analysis](snippets/sentiment.md)

+ [Adding images to ggplot2](snippets/ggplot-img.md)

+ [gganimate](snippets/gganimate.md)

+ [P-value simulation](snippets/pvalue-sim.md)

+ [Misc. snippets for data analysis/handling](snippets/data-snippets.md)

+ [Misc. snippets for research related topics/procedures](snippets/research-snippets.md)

<br><hr>

## Notes

+ [REGEX](notes/regex-basics.md)

+ [SQL](notes/sql-basics.md)

+ [GIT basics](notes/git-basics.md)

+ [GIT modifications](notes/git-mods.md)

<br><hr>

## Teaching

### My courses

+ [PSYC304: Statistics](teaching/stats/)

### General

+ [Teaching resources](teaching/teaching.md)

<br><hr>

## Misc

+ [Python notes](misc/python.md)

<br><br><br><hr>

*Inspiration for this repo: https://github.com/suzanbaert/Resources_and_Bookmarks*