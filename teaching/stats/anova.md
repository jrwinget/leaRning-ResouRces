    library(tidyverse)
    library(car)

### manually create fake data

    ID <- c(1:40)
    song <- c(rep(1, times = 10), rep(2, times = 10), rep(3, times = 10), rep(4, times = 10))
    temp <- c(rep(90, times = 5), rep(60, times = 5), rep(90, times = 5), rep(60, times = 5), rep(90, times = 5), rep(60, times = 5), rep(90, times = 5), rep(60, times = 5))
    mood <- c(rep(1, times =20), rep(2, times = 20))
    aggression_level <- c(2, 3, 1, 3, 4, 5, 5, 6, 7, 4, 6, 7, 8, 8, 7, 9, 10, 10, 9, 8, 2, 3, 1, 3, 4, 5, 5, 6, 7, 4, 6, 7, 8, 8, 7, 9, 10, 10, 9, 8)
    music <- data_frame(ID, song, temp, mood, aggression_level)

### make IVs factors (cateogrical)

    music$song <- factor(music$song)
    music$temp <- factor(music$temp)
    music$mood <- factor(music$mood)

### examine data

    View(music)
    glimpse(music)

### fit one-way anova model

    anova1 <- aov(aggression_level ~ song, data = music)

### evaluate model effects

    summary(anova1) # display Type I anova table
    drop1(anova1, ~ . , test = "F") # Type III SS and F Tests (this is the more accurate test!)
    TukeyHSD(anova1) # Tukey HSD post hoc tests

### plot results

    ggplot(data = music,
           aes(x = factor(song), # create x-axis variable (IV)
               y = aggression_level, # create y-axis variable (DV)
               color = factor(song))) + # color by IV
      geom_boxplot() + # shows where middle 50% of data are as well as outliers
      geom_point() + # data points
      geom_jitter(width = .25) # spread the data point so they don't overlap (since it's categorical)

LEARNING CHECK
--------------

-   Try creating a new plot with the same DV (aggression\_level) but
    with temp as the IV

### fit two-way anova model (+post hoc)

    anova2 <- aov(aggression_level ~ song + temp + song:temp, data = music)

### evaluate model effects

    summary(anova2) # display Type I anova table
    drop1(anova2, ~ . , test = "F") # Type III SS and F Tests (this is the more accurate test!)
    TukeyHSD(anova2) # Tukey HSD post hoc tests

LEARNING CHECK
--------------

-   Fit a new 2-way anova model using aov(aggression\_level ~ song +
    mood + song:mood, data = music), where mood is a new IV
-   Evaluate the model effects using summary, drop1, and TukeyHSD (if
    necessary)

### plot results

    ggplot(data = music,
           aes(x = factor(song), # create x-axis variable (first IV)
               y = aggression_level, # create y-axis variable (DV)
               color = factor(temp))) + # color by second IV
      geom_boxplot() + # shows where middle 50% of data are as well as outliers
      geom_point() + # data points
      geom_jitter(width = .25) # spread the data point so they don't overlap (since it's categorical)

### another way to plot 2-way anovas

    ggplot(data = music,
           aes(x = factor(song), # create x-axis variable (first IV)
               y = aggression_level, # create y-axis variable (DV)
               color = factor(song))) + # color by first IV
      facet_wrap(~ temp) + # split by second IV
      geom_boxplot() + # shows where middle 50% of data are as well as outliers
      geom_point() + # data points
      geom_jitter(width = .25) # spread the data point so they don't overlap (since it's categorical)

### create fake data for repeated measures ANOVA

    subject <- c(1:20)
    interest.t1 <- c(rep(1, times = 10), rep(2, times = 10))
    interest.t2 <- c(18, 21, 16, 22, 19, 24, 17, 21, 23, 18, 14, 16, 16, 19, 18, 20, 12, 22, 15, 17)
    interest.t3 <- c(22, 25, 17, 24, 16, 29, 20, 23, 29, 20, 15, 15, 18, 26, 18, 24, 18, 25, 19, 16)
    voting <- data_frame(subject, interest.t1, interest.t2, interest.t3)

    ageLevels <- c(1, 2, 3)
    ageFactor <- as.factor(ageLevels)
    ageFrame <- data.frame(ageFactor)
    ageBind <- cbind(voting$interest.t1, 
                     voting$interest.t2, 
                     voting$interest.t3)

    ageModel <- lm(ageBind ~ 1)

    analysis <- Anova(ageModel, idata = ageFrame, idesign = ~ ageFactor)

### evaluate model effects

    summary(analysis)
