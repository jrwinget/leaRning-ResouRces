    library(tidyverse)
    library(skimr)
    library(moderndive)

    # import data
    health <- read_csv("health-data.csv")

### explore the data frane

    View(health) # base r
    glimpse(health) # dplyr package
    kable(health) # skimr (or knitr) package

### more detailed examination of the data

    summary(health) # base r
    skim(health) # skimr package

    health %>% # this is called the pipe operator
      select(Income, DeathRate) %>% # select() comes from dplyr package
      summary() # give us an idea of how the values in both variables are distributed (but only univariate summaries)

### correlation coefficient

    # since we are interested in the relationship between 2 numerical variables, it'd be nice to have a summary stat that considers both vars
    # in other words, we want the correlation coefficient

    # we can get a quick snapshot of all correlations in a data frame by creating a correlation matrix
    cor(health, method = "pearson")

    # if we want to focus on one relationship (e.g., Income, DeathRate), we'd compute the correlation coeefficient using cor()
    cor(health$Income, health$DeathRate)

    # and we can test its significance using cor.test() - sig. testing is essentailly hypothesis testing 
    # (we'll talk about how this relates to correlations later)
    cor.test(x = health$Income, 
             y = health$DeathRate, 
             method = "pearson") # two-tailed test

### now, let’s visualize this relationship using a scatterplot - we will use geom\_point() from ggplot 2to create this plot

    ggplot(data = health,
           mapping = aes(x = Income,
                         y = DeathRate)) +
      geom_point() +
      labs(x = "Income",
           y = "Death rate",
           title = "Relationship between income and death rate")

    ggplot(data = health,
           mapping = aes(x = Income,
                         y = DeathRate)) +
      geom_point() +
      labs(x = "Income",
           y = "Death rate",
           title = "Relationship between income and death rate") +
      geom_smooth(method = "lm", se = FALSE) # this layer adds a line of best fit (regression line) to the plot

    ggplot(data = health,
           mapping = aes(x = Income,
                         y = DeathRate)) +
      geom_point() +
      labs(x = "Income",
           y = "Death rate",
           title = "Relationship between income and death rate") +
      geom_smooth(method = "lm") # remove the se argument to include standard error of the estimate bars around the regression line

LEARNING CHECK
--------------

-   Try creating a new plot with the same outcome variable (DeathRate)
    but with HospAvailability as the predictor variable

### fit the linear regression model such that we regress DeathRate onto PopDensity

    # the formula for the model is lm(y ~ x, data = data-frame-name)
    reg_model <- lm(DeathRate ~ PopDensity, data = health)

    # apply get_regression_table from moderndive package to reg_model
    get_regression_table(reg_model)

LEARNING CHECK
--------------

-   Fit a new simple linear regression using lm(DeathRate ~ Income, data
    = health), where Income is the new predictor var
-   Get info about the best-fitting line from the regression table. How
    do the regression results match up with the correlational example in
    the first part?

### determine beta for PopDensity

    summary(reg_model) # examine coefficents for PopDensity
    coeff.popdensity <- reg_model$coefficients["PopDensity"] # select PopDensity unstandardized regression coefficient and save it 
    beta.popdensity <- coeff.popdensity * sd(health$PopDensity) / sd(health$DeathRate) # use formula for unstd. reg. = B * (SDy / SDx)
    beta.popdensity # PopDensity standardized regression coefficient (beta)

### r^2 from summary output is only for the overall model, so we should calculate r^2 for individual varaibles (note: this is not the adj r^2)

    R2.popdensity <- beta.popdensity * cor(health$DeathRate, health$PopDensity) # use formula = beta * r 
    R2.popdensity

### graph the regression

    ggplot(data = health, 
           mapping = aes(x = PopDensity,
                         y = DeathRate)) +
      geom_point() +
      labs(x = "PopDensity",
           y = "Doctor availability",
           title = "Death rate regressed onto population density") +
      geom_smooth(method = "lm")
