### import data

    survey <- read_csv("yourfile.csv") # readr package

### explore the data frame

    View(survey) # base r
    glimpse(survey) # dplyr package
    kable(survey) # skimr (or knitr) package
    survey$Arcade # select variable with $ in base r

### more detailed examination of the data

    summary(survey) # base r
    skim(survey) # skimr package

LEARNING CHECK
--------------

-   What does any ONE row in the dataset refer to?
-   What are some examples of categorical variables in this dataset?
    What makes them different than quantitative variables?
-   What does int, dbl, and chr mean in the output from glimpse(survey)?

### histogram (for variability) for the Concerts variable using ggplot2 package

    ggplot(data = survey, 
           mapping = aes(x = Concerts)) +
      geom_histogram()

    ggplot(data = survey, 
           mapping = aes(x = Concerts)) +
      geom_histogram(bins = 10, # number of bins
                     color = "white") # helps distinguish columns

    ggplot(data = survey, 
           mapping = aes(x = Concerts)) +
      geom_histogram(bins = 10, 
                     color = "white", 
                     fill = "steelblue") # type colors() in the console to see all 657 available colors

    ggplot(data = survey, 
           mapping = aes(x = Concerts)) +
      geom_histogram(binwidth = 2, # number of units per bin
                     color = "white", 
                     fill = "steelblue")

LEARNING CHECK
--------------

-   What does changing the number of bins from 30 to 10 tell us about
    the distribtution of Concerts?
-   Would you classify the distribution of concerts as symmetric or
    skewed?
-   What would you guess is the “center” value in this distribution?
    Why?
-   Is this data spread out greatly from the center or is it close? Why?

### barplot (for distributions) for the Vacation variable using ggplot2 package

    # there are 2 ways of creating barplots, and they each depend on how the counts are represented in the data
    # for example, fruits data frame will not be pre-tabulated
    fruits <- data_frame(
      fruit = c("apple", "apple", "apple", "orange", "orange")
    )

    glimpse(fruits)

    # and fruits_counted will be pre-tabulated
    fruits_counted <- data_frame(
      fruit = c("apple", "orange"),
      number = c(3, 2)
    )

    glimpse(fruits_counted)

    # for data that are not pre-tabulated, use geom_bar
    ggplot(data = fruits, 
           mapping = aes(x = fruit)) +
      geom_bar()

    # for data that are pre-tabulated, use geom_col
    ggplot(data = fruits_counted, 
           mapping = aes(x = fruit, y = number)) +
      geom_col()

### now, consider the Vacation variable in the class survey (is is pre-tabulated or not?)

    glimpse(survey)

    ggplot(data = survey,
           mapping = aes(x = Vacation)) +
      geom_bar()

    vacation_table <- survey %>% # create table that displays count for each vacation location
      group_by(Vacation) %>% 
      summarize(number = n())

    vacation_table # this creates a pre-tabulated count of the Vacation variable

    ggplot(data = vacation_table,
           mapping = aes(x = Vacation, y = number)) +
      geom_col() # now we can use geom_col on this new vacation_table object

LEARNING CHECK
--------------

-   Why are histograms inappropriate for visualizing categorical
    varaibles?
-   What is the difference between histograms and barplots?
-   How many students in class would prefer to visit San Francisco?

### sometimes may want to compare more than one categorical variable in a barplot (do so with caution!)

    ggplot(data = survey,
           mapping = aes(x = Vacation, fill = Bear_Bees)) +
      geom_bar(position = "dodge")

### if you do not want the bear\_bees variable on the same graph, you can facet them

    ggplot(data = survey,
           mapping = aes(x = Vacation, fill = Bear_Bees)) +
      geom_bar() +
      facet_grid(Bear_Bees ~ .) # horizontal 

    ggplot(data = survey,
           mapping = aes(x = Vacation, fill = Bear_Bees)) +
      geom_bar() +
      facet_grid(. ~ Bear_Bees) # vertical
