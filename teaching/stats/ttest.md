    library(tidyverse)
    library(skimr)
    library(resahpe2)
    library(car)

### manually create fake data, students all from same class

    student <- c(1:20)
    grade_pre <- c(18, 21, 16, 22, 19, 24, 17, 21, 23, 18, 14, 16, 16, 19, 18, 20, 12, 22, 15, 17)
    grade_post <- c(22, 25, 17, 24, 16, 29, 20, 23, 29, 20, 15, 15, 18, 26, 18, 24, 18, 25, 19, 16)
    grades <- data_frame(student, grade_pre, grade_post)

### examine data

    glimpse(grades)
    skim(grades)

### one sample t-test, comparing grade\_post sample mean to grade\_post population mean of 25

    t.test(grades$grade_post, mu = 25) # H0: mu = 25

### paired samples t-test, comparing difference of grade\_pre sample mean and grade\_post sample mean

    t.test(grades$grade_pre, grades$grade_post, paired = TRUE)

LEARNING CHECK
--------------

-   What is the difference between the two t-tests we just ran?
-   Which mean was higher in the paired samples t-test?

### manaully create fake data, students from different classes

    student <- c(1:20)
    teacher <- c(rep(1, times = 10), rep(2, times = 10))
    grade_pre <- c(18, 21, 16, 22, 19, 24, 17, 21, 23, 18, 14, 16, 16, 19, 18, 20, 12, 22, 15, 17)
    grade_post <- c(22, 25, 17, 24, 16, 29, 20, 23, 29, 20, 15, 15, 18, 26, 18, 24, 18, 25, 19, 16)
    grades2 <- data_frame(student, teacher, grade_pre, grade_post)

### make categorical varialbes factors

    grades2$teacher <- factor(grades2$teacher)

### independent samples t-test

    t.test(grades2$grade_post ~ grades2$teacher)
    leveneTest(grade_post ~ teacher, grades2)

LEARNING CHECK
--------------

-   How would you interpret the output from the independent samples
    t-test?
