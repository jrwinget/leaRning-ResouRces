Useful snippets
===============

### Check for grammar and spelling

    devtools::spell_check() # check for spelling errors
    gramr::run_grammar_checker() # check for grammatical/stylistic errors

### Check for article retraction

    retractcheck::retractcheck("10.1016/j.annemergmed.2017.02.025") # using DOI
    retractcheck::retractcheck_pdf("/path/and/name-of.pdf") # using PDF
    retractcheck::retractcheck_docx("/path/and/name-of.docx") # using DOCX

### Check for stat errors

    statcheck::checkPDF("/path/and/name-of.pdf")

### Comparing the problem of HARKing to optimal stopping

    # HARKing, where x = number of tests and alpha = alpha level of tests
    harker <- function(x, alpha) {
      1 - (1 - alpha)^x
    }

    # Optimal stopping (accumulating)
    accumulator <- function(n.min, n.max, step, alpha = 0.05, iter = 10000) {

      # Determine places of peeks
      peeks <- seq(n.min, n.max, by = step)

      # Initialize result matrix (non-sequential)
      res <- matrix(NA, ncol = length(peeks), nrow = iter)
      colnames(res) <- peeks

      # Conduct sequential testing (always until n.max, with peeks at pre-determined places)
      for (i in 1:iter) {
        sample <- rnorm(n.max, 0, 1)
        res[i, ] <- sapply(peeks, FUN = function(x) {
          sum(sample[1:x]) / sqrt(x)
        })
      }

      # Create matrix: Which tests are significant?
      signif <- abs(res) >= qnorm(1 - alpha)

      # Create matrix: Has there been at least one significant test in the trial?
      seq.signif <- matrix(NA, ncol = length(peeks), nrow = iter)

      for (i in 1:iter) {
        for (j in 1:ncol(signif)) {
          seq.signif[i, j] <- TRUE %in% signif[i, 1:j]
        }
      }

      # Determine the sequential alpha error probability for the sequential tests
      seq.alpha <- apply(seq.signif, MARGIN = 2, function(x) {
        sum(x) / iter
      })

      # Return a list of individual test p-values, sequential significance and sequential alpha error probability
      return(list(seq.alpha = seq.alpha))
    }

    set.seed(1234567)

    # Optimal stopping false positive rates
    res.optstopp <- accumulator(n.min=10, n.max=100, step=10, alpha=0.025, iter=10000)

    print(res.optstopp[[1]])

    # HARKing False Positive Rates
    harker(x = 1:10, alpha = 0.05)
