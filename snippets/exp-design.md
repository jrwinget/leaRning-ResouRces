Experimental design
===================

Basics
------

    # basic experiment
    data(ToothGrowth)

    # Perform a two-sided t-test
    t.test(x = ToothGrowth$len, alternative = "two.sided", mu = 18)

Randomization
-------------

    # Perform a t-test
    ToothGrowth_ttest <- t.test(len ~ supp, data = ToothGrowth)

    # Load broom
    library(broom)

    # Tidy the t-test model object
    tidy(ToothGrowth_ttest)

Replication
-----------

    # Load dplyr
    library(dplyr)

    # Group by supp, dose, then examine how many observations in ToothGrowth there are by those groups
    ToothGrowth %>%
      group_by(supp, dose) %>%
      summarize(n = n())

Blocking
--------

    library(ggplot2)

    # Create a boxplot with geom_boxplot()
    ggplot(ToothGrowth, aes(x = dose, y = len)) +
      geom_boxplot()

    # Create the ToothGrowth_aov model object
    ToothGrowth_aov <- aov(len ~ dose + supp, data = ToothGrowth)

    # Examine the model object with summary()
    summary(ToothGrowth_aov)

One sided vs. two sided tests
-----------------------------

    # Test to see if the mean of the length variable of ToothGrowth is less than 18.
    t.test(
      x = ToothGrowth$len,
      alternative = c("less"),
      mu = 18
    )

    # greater than 18
    t.test(
      x = ToothGrowth$len,
      alternative = c("greater"),
      mu = 18
    )

Power and sample size calcuations (at least one argument needs to be NULL)
--------------------------------------------------------------------------

    # Load the pwr package
    library(pwr)

    # Calculate power
    pwr.t.test(
      n = 100, # for in each group
      d = 0.35,
      sig.level = 0.10,
      type = "two.sample",
      alternative = "two.sided",
      power = NULL
    )

    # Calculate sample size
    pwr.t.test(
      n = NULL,
      d = 0.25,
      sig.level = 0.05,
      type = "one.sample", alternative = "greater",
      power = 0.8
    )

Exploratory data analysis
-------------------------

    # Examine the variables with glimpse()
    glimpse(lendingclub)

    # Find median loan_amnt, mean int_rate, and mean annual_inc with summarise()
    lendingclub %>% summarize(median(loan_amnt), 
                              mean(int_rate), 
                              mean(annual_inc))

    # Use ggplot2 to build a bar chart of purpose
    ggplot(data = lendingclub, 
           aes(x = purpose)) + 
      geom_bar()

    # Use recode() to create the new purpose_recode variable
    lendingclub$purpose_recode <- lendingclub$purpose %>% recode(
      "credit_card" = "debt_related", "debt_consolidation" = "debt_related", "medical" = "debt_related",
      "car" = "big_purchase", "major_purchase" = "big_purchase", "vacation" = "big_purchase",
      "moving" = "life_change", "small_business" = "life_change", "wedding" = "life_change",
      "house" = "home_related", "home_improvement" = "home_related"
    )

How does loan purpose affect amount funded?
-------------------------------------------

    # Build a linear regression model, stored as purpose_recode_model
    purpose_recode_model <- lm(funded_amnt ~ purpose_recode, data = lendingclub)

    # Look at results of purpose_recode_model
    purpose_recode_model

    # Get anova results and save as purpose_recode_anova
    purpose_recode_anova <- anova(purpose_recode_model)

    # Look at the class of purpose_recode_anova
    purpose_recode_anova

Which loan purpose mean is different?
-------------------------------------

    # Use aov() to build purpose_recode_aov
    purpose_recode_aov <- aov(funded_amnt ~ purpose_recode, data = lendingclub)

    # Conduct Tukey's HSD test to create tukey_output
    tukey_output <- TukeyHSD(purpose_recode_aov, conf.level = 0.95)

    # Tidy tukey_output to make sense of the results
    tidy(tukey_output)

Multiple Factor Experiments
---------------------------

    # Use aov() to build purpose_emp_aov
    purpose_emp_aov <- aov(funded_amnt ~ purpose_recode + emp_length, data = lendingclub)

    # Print purpose_emp_aov to the console
    purpose_emp_aov

    # Call summary() to see the p-values
    summary(purpose_emp_aov)

Pre-modeling EDA
----------------

    # Examine the summary of int_rate
    summary(lendingclub$int_rate)

    # Examine int_rate by grade
    lendingclub %>% group_by(grade) %>% summarise(mean = mean(int_rate), var = var(int_rate), median = median(int_rate))

    # Make a boxplot of int_rate by grade
    ggplot(lendingclub, aes(x = grade, y = int_rate)) + geom_boxplot()

    # Use aov() to create grade_aov plus call summary() to print results to finally test the question
    grade_aov <- aov(int_rate ~ grade, data = lendingclub)
    summary(grade_aov)

Post-modeling validation plots + variance
-----------------------------------------

    # For a 2x2 grid of plots:
    par(mfrow=c(2, 2))

    # Plot grade_aov
    plot(grade_aov)

    # Bartlett's test for homogeneity of variance
    bartlett.test(int_rate ~ grade, data = lendingclub)

    # Conduct the Kruskal-Wallis rank sum test
    kruskal.test(int_rate ~ grade,
                 data = lendingclub)

Sample size for A/B testing
---------------------------

    # Use the correct function from pwr to find the sample size
    pwr.t.test(n = NULL, 
               d = 0.2, 
               power = 0.8, 
               sig.level = 0.05, 
               alternative = "two.sided")

Basic A/B test
--------------

    # Plot the a/b test results
    ggplot(lendingclub_ab, aes(x = Group, y = loan_amnt)) + geom_boxplot()

    # Conduct a two-sided t-test
    t.test(loan_amnt ~ Group, data = lendingclub_ab,
           alternative = c("two.sided"))

A/B tests vs. multivariable experiments
---------------------------------------

    # Build lendingclub_multi
    lendingclub_multi <-lm(loan_amnt ~ Group + grade + verification_status, data = lendingclub_ab)

    # Examine lendingclub_multi results
    tidy(lendingclub_multi)
