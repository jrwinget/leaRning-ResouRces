Useful snippet for larger programming concept
=============================================

### Nearly every formula and idea in stats in a mathematical shortcut for some number that you can get by simulation

To develop intuition on what some stat means, pick a computer language
and try to simulate it

For example, p-values represent the probably of getting some value at
least as extreme (far from null value) as the one you observed based on
repeated sampling if the “true” value was null. Complicated no matter
how you say it. To develop intuition, simulate it! For example, say we
rolled a die 100 times and it comes up six 24 times. We want to know how
sure we can be if the die was weighted. A p-value will tell us that.

### Real life version:

1.  Roll unweighted dice
2.  Record if it rolled a 6
3.  Repeate 1 and 2 about 100 times
4.  Record whether that set of 100 rolls had 24+ sixes
5.  Repeat 1-4 A LOT of time, say 100k times
6.  Record how many of those sets had 24+ sixes, and divide by 10k
7.  Boom! P-value

### Computers can do this faster and easier than we can do it by hand

    n_sixes <- 24
    n_total <- 100
    iterations <- 100000
    sets <- 0

    for(i in 1:iterations){
      sixes <- 0
      for(j in 1:n_total){
        dice.roll <- sample(1:6, 1)
        if(dice.roll == 6){sixes <- sixes + 1}
      }
      if(sixes >= n_sixes){sets = sets + 1}
    }
    p_val <- sets / iterations

*For practice, try making code shorter and more efficient.*

In the above, the “null” is 1/6, representing the probability of rolling
a six with an unweighted die. We checked to see how likely a set of
rolls with at least as many sixes as we observed with that unweighted
die. That’s what a p-value is.

In this case, the die seems likley to be weighted, as I would have
expected a result at least that big only 3.6% (this will vary based on
p-value run) of the time. However, 3.6% isn’t 0, and I could have gotten
24 sixes by random chance.
