Useful snippets for text analysis
=================================

### load packages and gather data

    library(rtweet)
    library(tidyverse)
    library(tidytext)
    library(ggraph)
    library(proustr)
    library(viridis)
    tweets <- search_tweets("#rstats", n = 2000, include_rts = FALSE)

### turn into one token per row

    glimpse(tweets)

    token_per_row <- tweets %>% 
      unnest_tokens(output = word, input = text)

    select(token_per_row, screen_name, word) %>% 
      slice(1:5)

### most common words

    token_per_row %>% 
      count(word) %>% 
      top_n(10, n) %>%
      ggplot() +
      aes(reorder(word, n), n) +
      geom_col(fill = viridis(10)[1]) +
      coord_flip() +
      labs(x = "word",
           y = "frequency") +
      theme_minimal()

### removing stop words

    token_per_row %>% 
      count(word) %>% 
      anti_join(stop_words) %>% 
      filter(! word %in% c("amp", "t.co", "https", "rstats")) %>% 
      top_n(10, n) %>% 
      ggplot() +
      aes(reorder(word, n), n) +
      geom_col(fill=viridis(1)) +
      coord_flip() +
      labs(x = "word",
           y = "frequency") +
      theme_minimal()

### do the same with bigrams

    #custom word stop list
    custm_stoplist <- c(stop_words$word, c("amp", "https", "t.co", "rstats"))

    bigrams <- tweets %>% 
      unnest_tokens(bigrams, text, token = "ngrams", n = 2) %>% 
      separate(bigrams, into = c("word1", "word2"), sep = " ") %>% 
      filter(! word1 %in% custm_stoplist) %>% 
      filter(! word2 %in% custm_stoplist) %>% 
      unite(bigrams, word1, word2, sep = " ")

### most common bigrams

    bigrams %>% 
      count(bigrams) %>% 
      top_n(10, n) %>% 
      ggplot() +
      aes(reorder(bigrams, n), n) +
      geom_col(fill=viridis(10)[3]) +
      coord_flip() +
      labs(x = "bigrams",
           y = "frequency") +
      theme_minimal()

### basic sentiment analysis

    get_sentiments("nrc")

    token_per_row %>% 
      inner_join(get_sentiments("nrc")) %>% 
      count(sentiment) %>% 
      ggplot() +
      aes(reorder(sentiment, n), n, fill = sentiment) +
      geom_col() +
      coord_flip() +
      scale_color_viridis() +
      theme_minimal() +
      labs(x = "sentiment",
           y = "frequency") +
      theme(legend.position = "none")

### temporal sentiment analysis

    proust_books() %>% 
      rownames_to_column() %>% 
      mutate(index = as.numeric(rowname) %/% 50) %>% 
      unnest_tokens(word, text) %>% 
      inner_join(proust_sentiments("polarity")) %>% 
      count(index, book, polarity) %>% 
      ggplot() +
      aes(index, n, fill = book) +
      geom_col() +
      facet_grid(polarity ~ .) +
      scale_fill_viridis_d() +
      theme_minimal() +
      theme(legend.position = "none")

### create corpus for ggraph

    gr_tweets <- tweets %>% 
      unnest_tokens(word, text) %>% 
      select(screen_name, word) %>% 
      filter(! word %in% custm_stoplist) %>% 
      pr_stem_words(word, language = "english")

    # create function
    who_says_that <- function(what) {
      filter(gr_tweets, word == what) %>% 
        igraph::graph_from_data_frame() %>% 
        ggraph() +
        geom_node_label(aes(label = name)) +
        geom_edge_link() +
        theme_graph()
    }

    # run the function
    who_says_that("dataviz")
    who_says_that("packag")
    who_says_that("teach")
