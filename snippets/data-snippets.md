Useful snippets
===============

### Save packages before upgrading R

    setwd("/your/path/to/temp/folder")
    getwd() # verify the change

    # before upgrade, build temp file with all of old packages
    tmp <- installed.packages()
    installedpkgs <- as.vector(tmp[is.na(tmp[,"Priority"]), 1])
    save(installedpkgs, file = "installed_old.rda")

    # install new version of R 
    # after new version is up, reload saved packages and reinstall them from CRAN
    tmp <- installed.packages()
    installedpkgs.new <- as.vector(tmp[is.na(tmp[, "Priority"]), 1])
    missing <- setdiff(installedpkgs, installedpkgs.new)
    install.packages(missing)
    update.packages()

### Reads all .csv files in a directory and place into single df

    f <- list.files(
      "my_folder",
      pattern = "*.csv",
      full.names = TRUE
    )
    d <- purrr::map_df(f,
                       readr::read_csv,
                       .id = "id")

### Batch convert .csv to .xslx

    library("rio")
    setwd("~/your/directory")
    csv <- dir(pattern = "csv")
    created <- mapply(convert, csv, gsub("csv", "xlsx", csv))
    unlink(csv)
    setwd("~/your/directory")

### Batch convert file names

    setwd("~/your/directory")
    file.rename(list.files(), stringr::str_replace(list.files(), "old-text", "new-text"))

### Data cleaning helper functions

    janitor::clean_names() # Cleans column names with spaces, special characters, and/or capitalization
    janitor::remove_empty() # remove all rows/columns from df that are composed entirely of NAs

### Custom data summarizer

    complete_summary <- function(df) {
      print(utils::str(df))
      print(skimr::skim(df))
      print(base::summary(df))
      print(dplyr::glimpse(df))
      plot(visdat::vis_dat(df))
    }

### Code to replace all NAs with a specified value

    purrr::map_dfr(ggplot2::msleep,
                   ~ tidyr::replace_na(., -1))

### Functions for checking distributions and outliers

    qk_freqploy <- function(df, x) {
      ggplot(df) +
        aes(x) +
        geom_freqpoly(aes(color = factor(cond)), binwidth = 1 / 4)
    }

    qk_boxplot <- function(df, x) {
      ggplot(df) +
        aes(factor(cond), x) +
        geom_boxplot() +
        xlab("condition")
    }

### Simple power function *need to test accuracy*

    power <- function(alpha, eff, n, m, l) { 
        df1 <- m - l 
        df2 <- n - m 
        c <- qf(1 - alpha, df1, df2) 
        lambda <- eff^2 * n 
        pow <- pf(c, df1, df2, ncp = lambda, lower.tail = FALSE) 
        return(pow) 
      }

### Z-test function *need to test accuracy*

    z.test <- function(a, mu, var){
      zeta = (mean(a) - mu) / (sqrt(var / length(a)))
      return(zeta)
    }

### Use broom to get a df of model results

    m <- lm(mpg ~ qsec + wt, data = mtcars)
    t <- broom::tidy(m)

    # glue the output into some nice sentences for a manuscript
    glue::glue_data(t,
                    "The point estimate for the {term} term is ",
                    "{round(estimate, 3)} ",
                    "(p-value = {signif(p.value, 2)}).")

    # use brm::file() to save fitted model to disk
    # brm will auto load the file from disk (instead of re-est model) on next run
    fit <- brm(
      outcome = ~ predictor,
      data = my_data,
      file = "fit-1"
    )

    # use hypothesis(scope = , group = ) to get transfrormed parameter values for groups in mulitlevel model 
    # ("general linear hyppthesis testing"; useful for meta-analyses)
    fit <- brm(
      Reaction ~ Days + (Days | Subject),
      data = sleepstudy,
      cores = 4,
      file = "sleepstudy"
    )
    question <- c("rt_day_4" = "Intercept + Days*4 = 0")
    (answer <- hypothesis(fit, question, scope = "coef", group = "Subject"))

    # use gr(subject, by = group) to model different between-subject covariance matrices for different groups in data
