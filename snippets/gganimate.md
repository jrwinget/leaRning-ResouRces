Useful snippets for gganimate
=============================

### Template

    library(tidyverse)
    library(gganimate)

    p <- ggplot() + stuff

    p + transition_time(year) +
      labs(title = `Year: {frame_time}`) +
      shadow_wake(
        wake_length = 0.1,
        alpha = FALSE
      ) # use for tracing old results or use shadow_mark() 

-   Transitions make the data change
-   Views make the viewpoint change
-   Shadows make the animation have memory
