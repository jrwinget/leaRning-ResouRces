    library(tidyverse)

SELECT
======

selecting based on pre-identified columns
-----------------------------------------

    columns <-  c("name", "genus", "vore", "order", "conservation")

    msleep %>% 
      select(!!columns) # could also use one_of() here instead of !!

by data data type (i.e., logical expression)
--------------------------------------------

    msleep %>% 
      select_if(is_numeric) %>% # select_if allows you to pass functions that return **logical statements**
      glimpse()

    # can also select the negation 
    # select_all/if/at functions require that a function is passed as an agrument, so have to wrap function inside funs() or add ~
    # when making funct on fly, usally need to refer to the value being replaced, which is represented by .
    msleep %>% 
      select_if(~!is.numeric(.)) %>% 
      glimpse()

    # can also select and modify
    msleep %>% 
      select_if(is_numeric, toupper)

    # other logical expressions
    # remember, requires that a function is passed as an agrument, so have to wrap function inside funs() or add ~
    msleep %>% 
      select_if(is.numeric) %>% 
      select_if(~mean(., na.rm = TRUE) > 10)

    # same thing, but shorter code
    msleep %>% 
      select_if(~is.numeric(.) & mean(., na.rm = TRUE) > 10)

    # another useful function for select_if is n_distinct, which counts # of distinct values in a column
    msleep %>% 
      select_if(~n_distinct(.) < 10)

reordering columns
==================

    # if just moving a couple of columns to front, can use everything()
    msleep %>% 
      select(conservation, sleep_total, everything()) %>% 
      glimpse()

column names
============

reformatting all column names
-----------------------------

    # select_all allows changes to all columns and takes a function as an argument
    msleep %>% 
      select_all(stringr::str_to_title)

    msleep %>%
      select_all(toupper)

    # can go a bit further and create functions on the fly
    # make unclean df for illustration
    msleep2 <- select(msleep, name, sleep_total, brainwt)

    # function on the fly to replace all white spaces with an underscore
    msleep2 %>% 
      select_all(~str_replace(., " ", "_"))

    # or, in case columns contain other metadata
    msleep2 <- select(msleep, name, sleep_total, brainwt)
    colnames(msleep2) <- c("Q1 name", "Q2 sleep total", "Q3 brain weight")
    msleep2[1:3,]

    # select_all and str_replace can be powerful
    msleep2 %>% 
      select_all(~str_replace(., "Q[0-9]+", "")) %>% 
      select_all(~str_replace(., " ", "_"))

row names to column
-------------------

    # sometimes, row names are actually a column in itself, like the mtcars df
    mtcars %>% 
      head()

    # to make this an actual column, use rownames_to_column()
    mtcars %>% 
      tibble::rownames_to_column("model") %>% 
      head()

MUTATE
======

some basics
-----------

    # ifelse() is useful if you don't want to mutate the whole column in the same way
    # specify logical statment, specify what happens when TRUE, specify what happens when FALSE
    msleep %>% 
      select(name, brainwt) %>% 
      mutate(brainwt2 = ifelse(
        brainwt > 4, NA, brainwt
      )) %>% 
      arrange(desc(brainwt))

    # mutate columns with str_extract in combo with regex patterns
    msleep %>% 
      select(name) %>% 
      mutate(name_last_word = tolower(str_extract(name, pattern = "\\w+$")))

mutate\_all
-----------

    # pass function to apply to all columns
    # rather than affecting columm names (like select_all), this will change the actual data values
    msleep %>% 
      mutate_all(tolower)

    # can also make up functions on the fly, but must wrap in funs() or add ~
    # simulate web scraped data with extra spaces and "/n"
    msleep_web <- msleep %>% 
      mutate_all(~paste(., "    /n   "))
    msleep_web[,1:4]

    # clean it up with mutate_all, while assuming not all whitespace is the same
    msleep_clean <- msleep_web %>% 
      mutate_all(~str_replace(., "/n", "")) %>% 
      mutate_all(str_trim)
    msleep_clean[,1:4]

mutate\_if
----------

    # cannot apply all funct with mutate_all (eg, round throws an error bc there is num AND chr columns)
    # so, need to specify cond that columns need to be num before giving round() instructions, using mutate_if
    msleep %>% 
      select(name, sleep_total:bodywt) %>% 
      mutate_if(is.numeric, round)

mutate\_at
----------

    # requires (1) info about columns to consider wrapped inside vars(), (2) instructions about mutation in form of function (if needed use funs() or a ~)
    msleep %>% 
      select(name, sleep_total:awake) %>% 
      mutate_at(vars(contains("sleep")), ~(.*60)) # multiples all columns containing "sleep" by 60 (i.e., changed to minutes)

changing column names after mutation
------------------------------------

    # confusing that the sleep columns keep the same name but are now in a different unit, so we can rename_at
    msleep %>%
      select(name, sleep_total:awake) %>%
      mutate_at(vars(contains("sleep")), ~(.*60)) %>%
      rename_at(vars(contains("sleep")), ~paste0(.,"_min"))

    # or use a take within the funs() call of mutate_at, but this will keep all variables
    msleep %>%
      select(name, sleep_total:awake) %>%
      mutate_at(vars(contains("sleep")), funs(min = .*60))

recoding descrete columns
-------------------------

    # using recode() inside mutate() allow one to change the current naming or to group current levels into less levels
    # .default refers to anything that isn't covered by the before groups with the exception of NA
    msleep %>% count(conservation)

    msleep %>% 
      mutate(conservation2 = recode(conservation,
                                    "en" = "Endangered",
                                    "lc" = "Least_Concern",
                                    "domesticated" = "Least_Concern",
                                    .default = "other")) %>% 
      count(conservation2)

    # special version of recode to return a factor: recode_factor()
    # .ordered argument is FALSE by default, set to TRUE to return ordered factor set
    # can change NA into something else by adding a .missing argument 
    msleep %>% 
      mutate(conservation2 = recode_factor(conservation,
                                           "en" = "Endangered",
                                           "lc" = "Least_Concern",
                                           "domesticated" = "Least_Concern",
                                           .default = "other",
                                           .missing = "no data",
                                           .ordered = TRUE)) %>% 
      count(conservation2)

creating new 2 level descrete columns
-------------------------------------

    # ifelse can be used to turn a num column into a discrete one
    msleep %>% 
      select(name, sleep_total) %>% 
      mutate(sleep_time = ifelse(sleep_total > 10, "long", "short"))

creating new multi level descrete columns
-----------------------------------------

    # ifelse cant be nested for more than 2 levels, but it's probably easier to use case_when for these situations
    # arguments are evaluated in order, everything at end is represented with TRUE
    # not really an easy way to return an ordered factor, so use fct_relevel or factor afterwards (for lots of levels, make vector)
    msleep %>%
      select(name, sleep_total) %>%
      mutate(sleep_total_discr = case_when(
        sleep_total > 13 ~ "very long",
        sleep_total > 10 ~ "long",
        sleep_total > 7 ~ "limited",
        TRUE ~ "short")) %>%
      mutate(sleep_total_discr = factor(sleep_total_discr, 
                                        levels = c("short", "limited", 
                                                   "long", "very long")))

    # case_when also works for grouping across columns
    msleep %>% 
      mutate(silly_groups = case_when(
        brainwt < 0.001 ~ "light_headed",
        sleep_total > 10 ~ "lazy sleeper",
        is.na(sleep_rem) ~ "absent_rem",
        TRUE ~ "other"
      )) %>% 
      count(silly_groups)

SEPARATE and UNITE
------------------

    (conserv_abbrv <- as_tibble(
    c("EX = Extinct",
    "EW = Extinct in the wild",
    "CR = Critically Endangered",
    "EN = Endangered",
    "VU = Vulnerable",
    "NT = Near Threatened",
    "LC = Least Concern",
    "DD = Data deficient",
    "NE = Not evaluated",
    "PE = Probably extinct (informal)",
    "PEW = Probably extinct in the wild (informal)")))

    # unmerge columns using separate()
    (conserv_table <- conserv_abbrv %>% 
      separate(value,
               into = c("abbreviation", "description"), sep = " = "))

    # or merge two columns togeter
    conserv_table %>% 
      unite(united,
            abbreviation, description, sep = ": ")

brining columns in from other df (join functions)
-------------------------------------------------

    # using left_join in this case (keep main table on left, added columns on right)
    # in by = statement, specify which columns are the same in each df so join knows what to add where
    msleep %>% 
      select(name, conservation) %>% 
      mutate(conservation = toupper(conservation)) %>% 
      left_join(conserv_table, by = c("conservation" = "abbreviation")) %>% 
      mutate(description = ifelse(is.na(description), conservation, description))

SPREAD and GATHER
-----------------

    # gather many columns into one
    # needs a name for the new descriptive column (key): here, previous column names become values
    # needs a name for the value column (value): here, the previous values are placed into a single column
    # columns you don't want to gather need to be deselected at the end (or the ones to gather need to be selected)
    msleep %>% 
      select(name, contains("sleep")) %>% 
      gather(key = "sleep_measure",
             value = "time",
             -name)

    # in prev code, sleep_measure is a chr order alphabetically
    # to make ordered factor, set factor_key argument to TRUE
    (msleep_g <- msleep %>%
      select(name, contains("sleep")) %>%
      gather(key = "sleep_measure", value = "time", -name, factor_key = TRUE))

    # spread will take one column and make it multiple
    msleep_g %>% 
      spread(sleep_measure, time)

turning data in NA
------------------

    # na_if() turns particular values into NA
    # in most cases, will probably use na_if(" "), turns empty string into NA, but can do anything
    msleep %>% 
      select(name:order) %>% 
      na_if("omni")

FILTER
======

basics
------

    # between() helper function can come in handy
    msleep %>% 
      select(name, sleep_total) %>% 
      filter(between(sleep_total, 16, 18))

    # near() selects all code that is nearly a given value
    # must specify tolerance (tol =) to indicate how far the values can be
    msleep %>% 
      select(name, sleep_total) %>% 
      filter(near(sleep_total, 17, tol = sd(sleep_total)))

extract chr matches
-------------------

    # to select more than one value, use %in% operator
    msleep %>% 
      select(order, name, sleep_total) %>% 
      filter(order %in% c("Didelphimorphia", "Diprotodontia"))

    # negating with %in% needs to happen at the beginning of the filter
    remove <- c("Rodentia", "Carnivora", "Primates")
    msleep %>% 
      select(order, name, sleep_total) %>% 
      filter(!order %in% remove)

filter based on regex
---------------------

    # str_detect is the tidyverse equivalent of grepl()
    # filters based on partial matches
    msleep %>% 
      select(name, sleep_total) %>% 
      filter(str_detect(tolower(name), pattern = "mouse"))

filter based on multiple conditions
-----------------------------------

    # The above examples return rows based on a single condition, but the filter option also allows AND and OR style filters:
    # filter(condition1, condition2) returns rows where both conditions are met
    # filter(condition1, !condition2) returns all rows where condition one is true but condition 2 is not
    # filter(condition1 | condition2) returns rows where condition 1 and/or condition 2 is met
    # filter(xor(condition1, condition2) returns all rows where only one of the conditions is met and not when both conditions are met
    # multiple AND, OR, and NOT conditions can be combined
    msleep %>% 
      select(name, order, sleep_total:bodywt) %>% 
      filter(bodywt > 100, (sleep_total > 15 | order != "Carnivora"))

    # example with xor()
    msleep %>%
      select(name, bodywt:brainwt) %>% 
      filter(xor(bodywt > 100, brainwt > 1))

    # example with !
    msleep %>% 
      select(name, sleep_total, brainwt, bodywt) %>% 
      filter(brainwt > 1, !bodywt > 100)

filter out empty rows
---------------------

    # to filter entire rows containing NAs
    msleep %>% 
      select(name, conservation:sleep_cycle) %>% 
      filter(!is.na(conservation))

    # or just use drop_na()
    msleep %>% 
      select(name, conservation:sleep_cycle) %>% 
      drop_na(conservation)

    # can also use drop_na to exclude rows with NAs from ANY column (ie, all NAs)
    msleep %>% 
      select(name, conservation:sleep_cycle) %>% 
      drop_na()

filter\_all
-----------

    # filter for rows that have a certain word string in ANY column
    msleep %>% 
      select(name:order, sleep_total, -vore) %>% 
      filter_all(any_vars(str_detect(., pattern = "Ca")))

    # same can be done for num values
    # filter rows that have any value below 0.1
    msleep %>% 
      select(name, sleep_total:bodywt) %>% 
      filter_all(any_vars(. < 0.1))

    # any_vars() is equivalent to OR
    # all_vars() is equivalent to AND
    msleep %>% 
      select(name, sleep_total:bodywt, -awake) %>% 
      filter_all(all_vars(. > 1))

filter\_if
----------

    # To find all data rows where NA in the first few columns, filter_all(any_vars(is.na(.))) will be useless, returns 27 rows many of which are missing data in the measurement section.
    # In this case: filter_if() comes in handy. The describing columns are all character columns, while the measurement data is numeric. So using filter_if() I can specify that I want to just filter on character variables. In this case I only get 7 rows.

    msleep %>% 
      select(name:order, sleep_total:sleep_rem) %>% 
      filter_if(is.character, any_vars(is.na(.)))

filter\_all
-----------

    # select columns to which the change should happen via vars()
    msleep %>% 
      select(name, sleep_total:sleep_rem, brainwt:bodywt) %>% 
      filter_at(vars(sleep_total, sleep_rem), all_vars(.>5))

    msleep %>% 
      select(name, sleep_total:sleep_rem, brainwt:bodywt) %>% 
      filter_at(vars(contains("sleep")), all_vars(.>5))

SUMMARIZE
=========

counting \# of observations
---------------------------

    msleep %>% 
      count(order, sort = TRUE)

    # can add multiple variables to count()
    msleep %>% 
      count(order, vore, sort = TRUE)

adding the number of observations in a column
---------------------------------------------

    # tally() is the rough tidyverse equivalent of nrow()
    # can't provide a variable to count with tally(), only works on overall number of observations
    # count() is a short-hand for group_by() and tally()
    msleep %>% 
      tally()

    # add_tally() automatically adds a column with the overall number of observations
    # same as mutate(n = n())
    msleep %>% 
      select(1:3) %>% 
      add_tally()

    # add_count() takes variable as argument and adds column 
    # same as group_by() %>% add_tally()
    # saves the combination of grouping, mutating, and ungrouping again
    msleep %>% 
      select(name:vore) %>% 
      add_count(vore)

summarize\_all
--------------

    # requires function as an agrument, which is applied to all columns
    msleep %>% 
      group_by(vore) %>% 
      summarize_all(mean, na.rm = TRUE)

    # can also make up functions on the fly
    # remember to wrap in funs() or add ~
    msleep %>% 
      group_by(vore) %>% 
      summarize_all(~mean(., na.rm = TRUE) + 5)

summarize\_if
-------------

    # needs to know information about columns (eg, is.numeric)
    # needs information about how to summarize that data, which needs to be a function (wrapped in funs() or a ~)
    msleep %>% 
      group_by(vore) %>% 
      summarize_if(is.numeric, mean, na.rm = TRUE)

    # useful to rename what these new values are
    msleep %>% 
      group_by(vore) %>% 
      summarize_if(is.numeric, mean, na.rm = TRUE) %>% 
      rename_if(is.numeric, ~paste0("avg_", .))

summarize\_at
-------------

    # needs information about the columns to consider
    # needs information about how to summarize that data, which has to be a function (wrapped in funs() or with a ~)
    msleep %>% 
      group_by(vore) %>% 
      summarize_at(vars(contains("sleep")), mean, na.rm = TRUE) %>% 
      rename_at(vars(contains("sleep")), ~paste0("avg_", .))

arranging rows
--------------

    # if data are already grouped, can refer to group within arrange()
    msleep %>%
      select(order, name, sleep_total) %>%
      group_by(order) %>%
      arrange(desc(sleep_total), .by_group = TRUE)
