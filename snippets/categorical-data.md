Categorical data (forcats)
==========================

Basics
------

### Recognize factors

    # Print out the dataset
    print(multiple_choice_responses)

    # Check if CurrentJobTitleSelect is a factor
    is.factor(multiple_choice_responses$CurrentJobTitleSelect)

### Get number of levels

    # Change all the character columns to factors
    responses_as_factors <- multiple_choice_responses %>%
      mutate_if(is.character, as.factor)

    # Make a two column dataset with variable names and number of levels
    number_of_levels <- responses_as_factors %>%
      summarise_all(nlevels) %>% # change df to one row per variable, where each entry is the # of levels for that variable
      gather(variable, num_levels) # change df from wide to long

### Examine number of levels

    # top_n() will give first x rows of a df based on a var, whereas pull() allows us to extract a solumn and take out the name, leaving only the values from the column

    # Select the 3 rows with the highest number of levels
    number_of_levels %>%
      top_n(3, num_levels)

    # How many levels does CurrentJobTitleSelect have?
    number_of_levels %>%
      filter(variable == "CurrentJobTitleSelect") %>%
      pull(num_levels)

    # Get the names of the levels of CurrentJobTitleSelect
    responses_as_factors %>%
      pull(CurrentJobTitleSelect) %>% # pull can be used interchangably with $ as a way to extract a column as a vector
      levels()

### Making better plots

    # Make a bar plot
    ggplot(multiple_choice_responses, aes(x = EmployerIndustry)) +
      geom_bar() +
      # flip the coordinates
      coord_flip()

    # Now make the bar plot ordered from top to bottom, highest to lowest count
    ggplot(multiple_choice_responses, aes(x = fct_rev(fct_infreq(EmployerIndustry)))) +
      geom_bar() +
      coord_flip()

    # order one variable by another
    multiple_choice_responses %>%
      # remove NAs
      filter(
        !is.na(Age),
        !is.na(EmployerIndustry)
      ) %>%
      # get mean_age by EmployerIndustry
      group_by(EmployerIndustry) %>%
      summarize(mean_age = mean(Age)) %>%
      # make a scatterplot of EmployerIndustry by mean_age
      ggplot(aes(x = fct_reorder(EmployerIndustry, mean_age), y = mean_age)) +
      geom_point() +
      coord_flip()

Manipulating factor variables
-----------------------------

### Changing the order of factor levels

    # Print the levels of WorkInternalVsExternalTools
    print(multiple_choice_responses$WorkInternalVsExternalTools)

    # Reorder the levels from internal to external
    mc_responses_reordered <- multiple_choice_responses %>%
      mutate(WorkInternalVsExternalTools = fct_relevel(
        WorkInternalVsExternalTools,
        "Entirely internal", "More internal than external", "Approximately half internal and half external", "More external than internal", "Entirely external", "Do not know"
      ))

    # Make a bar plot of the responses
    ggplot(data = mc_responses_reordered, aes(x = WorkInternalVsExternalTools)) +
      geom_bar() +
      coord_flip()

### Tricks of fct\_relevel() - instead of writing out the name of each level, you can use helper arguments in fct\_relevel

    # demonstrate how to use these tricks
    multiple_choice_responses %>%
      # Move "I did not complete any formal education past high school" and "Some college/university study without earning a bachelor's degree" to the front
      mutate(FormalEducation = fct_relevel(FormalEducation, "I did not complete any formal education past high school", "Some college/university study without earning a bachelor's degree")) %>%
      # Move "I prefer not to answer" to be the last level.
      mutate(FormalEducation = fct_relevel(FormalEducation, "I prefer not to answer", after = Inf)) %>%
      # Move "Doctoral degree" to be the sixth level
      mutate(FormalEducation = fct_relevel(FormalEducation, "Doctoral degree", after = 5)) %>%
      # Examine the new level order
      pull(FormalEducation) %>%
      levels()

    # renaming a few levels
    # make a bar plot of the frequency of FormalEducation
    ggplot(multiple_choice_responses, aes(x = FormalEducation)) +
      geom_bar()

    # Now rename levels
    multiple_choice_responses %>%
      # rename levels
      mutate(FormalEducation = fct_recode(FormalEducation,
        "High school" = "I did not complete any formal education past high school",
        "Some college" = "Some college/university study without earning a bachelor's degree"
      )) %>%
      # make a bar plot of FormalEducation
      ggplot(aes(x = FormalEducation)) +
      geom_bar()

    # collapsing levels
    # there are 16 job tiles, so let's collapse these into bigger categories
    multiple_choice_responses %>%
      # Create new variable, grouped_titles, by collapsing levels in CurrentJobTitleSelect
      mutate(grouped_titles = fct_collapse(CurrentJobTitleSelect,
        "Computer Scientist" = c("Programmer", "Software Developer/Software Engineer"),
        "Researcher" = "Scientist/Researcher",
        "Data Analyst/Scientist/Engineer" = c(
          "DBA/Database Engineer", "Data Scientist",
          "Business Analyst", "Data Analyst",
          "Data Miner", "Predictive Modeler"
        )
      )) %>%
      # Turn every title that isn't now one of the grouped_titles into "Other"
      mutate(grouped_titles = fct_other(grouped_titles,
        keep = c(
          "Computer Scientist",
          "Researcher",
          "Data Analyst/Scientist/Engineer"
        )
      )) %>%
      # Get a count of the grouped titles
      count(grouped_titles)

    # lumping variables by proportion (bc sometimes there won't be specifc levels you want to create)
    multiple_choice_responses %>%
      # remove NAs of MLMethodNextYearSelect
      filter(!is.na(MLMethodNextYearSelect)) %>%
      # create ml_method, which lumps all those with less than 5% of people into "Other"
      mutate(ml_method = fct_lump(MLMethodNextYearSelect, prop = .05)) %>%
      # print the frequency of your new variable in descending order
      count(ml_method, sort = TRUE)

    # preserving the most common levels (using freqency rather than proportion)
    multiple_choice_responses %>%
      # remove NAs
      filter(!is.na(MLMethodNextYearSelect)) %>%
      # create ml_method, retaining the 5 most common methods and renaming others "other method"
      mutate(ml_method = fct_lump(MLMethodNextYearSelect, n = 5, other_level = "other method")) %>%
      # print the frequency of your new variable in descending order
      count(ml_method, sort = TRUE)

Creating factor variables
-------------------------

    # sometimes, multiple columns are tied together and we want to examine them together. first, we need to find them and change them into aformat that is easier to use
    learning_platform_usefulness <- multiple_choice_responses %>%
      # select columns with LearningPlatformUsefulness in title
      select(contains("LearningPlatformUsefulness")) %>%
      # change data from wide to long
      gather(learning_platform, usefulness) %>%
      # remove rows where usefulness is NA
      filter(!is.na(usefulness)) %>%
      # remove "LearningPlatformUsefulness" from each string in `learning_platform
      mutate(learning_platform = str_remove(learning_platform, "LearningPlatformUsefulness"))

Summarizing variables
---------------------

    # Use a single dplyr function to change the dataset to have one row per learning_platform usefulness pair with a column that is the number of entries with that pairing.
    learning_platform_usefulness %>%
      # change dataset to one row per learning_platform usefulness pair with number of entries for each
      count(learning_platform, usefulness)

    # Use add_count() to create a column with the total number of answers to that learning_platform
    learning_platform_usefulness %>%
      # change dataset to one row per learning_platform usefulness pair with number of entries for each
      count(learning_platform, usefulness) %>%
      # use add_count to create column with total number of answers for that learning_platform
      add_count(learning_platform, wt = n)

    # Create a new column, perc, that's the percent of people giving a certain answer for that question. Save everything as a new dataset, perc_useful_platform
    perc_useful_platform <- learning_platform_usefulness %>%
      # change dataset to one row per learning_platform usefulness pair with number of entries for each
      count(learning_platform, usefulness) %>%
      # use add_count to create column with total number of answers for that learning_platform
      add_count(learning_platform, wt = n) %>%
      mutate(perc = n / nn)

    # For each learning platform, create a line graph with usefulness on the x-axis and percentage of responses within the learning platforms on the y-axis
    ggplot(perc_useful_platform, aes(x = usefulness, y = perc, group = learning_platform)) +
      geom_line() +
      facet_wrap(~learning_platform)

Refine plot so that all data are in one graph
---------------------------------------------

    # Change usefulness to be a binary variable, equaling 0 if someone answered "Not Useful" and 1 otherwise
    usefulness_by_platform <- learning_platform_usefulness %>%
      # If usefulness is "Not Useful", make 0, else 1
      mutate(usefulness = if_else(usefulness %in% c("Not Useful"), 0, 1)) %>%
      # Get the average usefulness by learning platform
      group_by(learning_platform) %>%
      summarize(avg_usefulness = mean(usefulness))

    # Make a scatter plot of average usefulness by learning platform
    ggplot(usefulness_by_platform, aes(x = learning_platform, y = avg_usefulness)) +
      geom_point()

    # editing plot text
    ggplot(usefulness_by_platform, aes(x = learning_platform, y = avg_usefulness)) +
      geom_point() +
      # rotate x-axis text by 90 degrees
      theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
      # rename y and x axis labels
      labs(x = "Learning Platform", y = "Percent finding at least somewhat useful") +
      # change y axis scale to percentage
      scale_y_continuous(labels = scales::percent)

    # reordering graph - order learning_platform in the graph by avg_usefulness so that, from left to right, it goes from highest usefulness to lowest
    ggplot(usefulness_by_platform, aes(fct_rev(fct_reorder(learning_platform, avg_usefulness)), y = avg_usefulness)) +
      geom_point() +
      theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
      labs(x = "Learning Platform", y = "Percent finding at least somewhat useful") +
      scale_y_continuous(labels = scales::percent)

Make a categorical variable from single vector/column using case\_when()
------------------------------------------------------------------------

    # focus on rows where age is above 10 and below 10, create new column called generation, get count of people in each generation
    multiple_choice_responses %>%
      # Eliminate any ages below 10 and above 90
      filter(between(Age, 10, 90)) %>%
      # Create the generation variable based on age
      mutate(generation = case_when(
        between(Age, 10, 22) ~ "Gen Z",
        between(Age, 23, 37) ~ "Gen Y",
        between(Age, 38, 52) ~ "Gen X",
        between(Age, 53, 71) ~ "Baby Boomer",
        between(Age, 72, 90) ~ "Silent"
      )) %>%
      # Get a count of how many answers in each generation
      count(generation)

case\_when() for multiple columns
---------------------------------

    # remove people who selected data scientist, create a new var based on current job title and identity, get average job satisfaction
    multiple_choice_responses %>%
      # Filter out people who selected Data Scientist as their Job Title
      filter(CurrentJobTitleSelect != "Data Scientist") %>%
      # Create a new variable, job_identity
      mutate(job_identity = case_when(
        CurrentJobTitleSelect == "Data Analyst" &
          DataScienceIdentitySelect == "Yes" ~ "DS analysts",
        CurrentJobTitleSelect == "Data Analyst" &
          DataScienceIdentitySelect %in% c("No", "Sort of (Explain more)") ~ "NDS analyst",
        CurrentJobTitleSelect != "Data Analyst" &
          DataScienceIdentitySelect == "Yes" ~ "DS non-analysts",
        TRUE ~ "NDS non analysts"
      )) %>%
      # Get the average job satisfaction by job_identity, removing NAs
      group_by(job_identity) %>%
      summarize(avg_js = mean(JobSatisfaction, na.rm = TRUE))

Putting it all together
-----------------------

    # case study
    gathered_data <- flying_etiquette %>%
      # Change characters to factors
      mutate_if(is.character, as.factor) %>%
      # Filter out those who have never flown on a plane
      filter(`How often do you travel by plane?` != "Never") %>%
      # Select columns containing "rude"
      select(contains("rude")) %>%
      # Change format from wide to long
      gather(response_var, value)

    # regular expressions: . = matches any character; * = zero or more times
    gathered_data %>%
      # Remove everything before and including "rude to "
      mutate(response_var = str_remove(response_var, ".*rude to ")) %>%
      # Remove "on a plane"
      mutate(response_var = str_remove(response_var, "on a plane"))

    # dichotomizing variables
    dichotimized_data <- gathered_data %>%
      mutate(response_var = str_replace(response_var, ".*rude to ", "")) %>%
      mutate(response_var = str_replace(response_var, "on a plane", "")) %>%
      # Remove rows that are NA in the value column
      filter(!is.na(value)) %>%
      # Dichotomize the value variable to make a new variable, rude
      mutate(rude = if_else(value %in% c("No, not rude at all", "No, not at all rude"), 0, 1))

    # summarizing data
    # create 2 columns: the question (response var) and the mean of the rude column (perc_rude)
    rude_behaviors <- gathered_data %>%
      mutate(response_var = str_replace(response_var, ".*rude to ", "")) %>%
      mutate(response_var = str_replace(response_var, "on a plane", "")) %>%
      # Remove rows that are NA in the value column
      filter(!is.na(value)) %>%
      mutate(rude = if_else(value %in% c("No, not rude at all", "No, not at all rude"), 0, 1)) %>%
      # Create perc_rude, the percent considering each behavior rude
      group_by(response_var) %>%
      summarize(perc_rude = mean(rude))

    rude_behaviors

    # creating an initial plot
    # Create an ordered by plot of behavior by percentage considering it rude
    initial_plot <- ggplot(rude_behaviors, aes(x = fct_reorder(response_var, perc_rude), y = perc_rude)) +
      geom_col()

    # View your plot
    initial_plot

    # fix the labels
    titled_plot <- initial_plot +
      # Add the title, subtitle, and caption
      labs(
        title = "Hell Is Other People In A Pressurized Metal Tube",
        subtitle = "Percentage of 874 air-passenger respondents who said action is very or somewhat rude",
        caption = "Source: SurveyMonkey Audience",
        # Remove the x- and y-axis labels
        x = "",
        y = ""
      )

    # view plot
    titled_plot

    # flipping things around
    flipped_plot <- titled_plot +
      # Flip the axes
      coord_flip() +
      # Remove the x-axis ticks and labels
      theme(
        axis.ticks.x = element_blank(),
        axis.text.x = element_blank()
      )

    # Add a label to each bar with the percent rude value with a % sign (e.g. "85%")
    flipped_plot +
      # Add labels above the bar with the perc value
      geom_text(aes(
        label = percent(perc_rude),
        y = perc_rude + .03
      ),
      position = position_dodge(0.9),
      vjust = 1
      )
