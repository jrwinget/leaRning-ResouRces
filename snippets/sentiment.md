Useful snippets for sentiment analysis
======================================

    library(geniusr)
    library(tidyverse)
    library(tidytext)
    hotel_calif <- genius_lyrics(artist = "Eagles", song = "Hotel California") %>%
      mutate(line = row_number())

### First, we’ll chop up these 43 lines into individual words, using the tidytext package and unnest\_tokens function.

    tidy_hc <- hotel_calif %>%
      unnest_tokens(word,lyric)

### create df

    new_sentiments <- sentiments %>%
      mutate(sentiment = ifelse(lexicon == "AFINN" & score >= 0, "positive",
                                ifelse(lexicon == "AFINN" & score < 0,
                                       "negative", sentiment))) %>%
      group_by(lexicon) %>%
      mutate(words_in_lexicon = n_distinct(word)) %>%
      ungroup()

### see how lexicon compares with words in lyrics

    my_kable_styling <- function(dat, caption) {
      kable(dat, "html", escape = FALSE, caption = caption) %>%
        kable_styling(bootstrap_options = c("striped", "condensed", "bordered"),
                      full_width = FALSE)
    }


    library(kableExtra)
    library(formattable)
    library(yarrr)
    tidy_hc %>%
      mutate(words_in_lyrics = n_distinct(word)) %>%
      inner_join(new_sentiments) %>%
      group_by(lexicon, words_in_lyrics, words_in_lexicon) %>%
      summarise(lex_match_words = n_distinct(word)) %>%
      ungroup() %>%
      mutate(total_match_words = sum(lex_match_words),
             match_ratio = lex_match_words/words_in_lyrics) %>%
      select(lexicon, lex_match_words, words_in_lyrics, match_ratio) %>%
      mutate(lex_match_words = color_bar("lightblue")(lex_match_words),
             lexicon = color_tile("lightgreen","lightgreen")(lexicon)) %>%
      my_kable_styling(caption = "Lyrics Found In Lexicons")

### nrc has best match

    hcsentiment <- tidy_hc %>%
      inner_join(get_sentiments("nrc"), by = "word")

    hcsentiment

### viz counts of different emotions and sentiments in nrc

    theme_lyrics <- function(aticks = element_blank(),
                             pgminor = element_blank(),
                             lt = element_blank(),
                             lp = "none")
    {
      theme(plot.title = element_text(hjust = 0.5), #Center the title
            axis.ticks = aticks, #Set axis ticks to on or off
            panel.grid.minor = pgminor, #Turn the minor grid lines on or off
            legend.title = lt, #Turn the legend title on or off
            legend.position = lp) #Turn the legend on or off
    }

    hcsentiment %>%
      group_by(sentiment) %>%
      summarise(word_count = n()) %>%
      ungroup() %>%
      mutate(sentiment = reorder(sentiment, word_count)) %>%
      ggplot(aes(sentiment, word_count, fill = -word_count)) +
      geom_col() +
      guides(fill = FALSE) +
      theme_minimal() + theme_lyrics() +
      labs(x = NULL, y = "Word Count") +
      ggtitle("Hotel California NRC Sentiment Totals") +
      coord_flip()

### Most of the words appear to be positively-valenced. How do the individual words match up?

    library(ggrepel)

    plot_words <- hcsentiment %>%
      group_by(sentiment) %>%
      count(word, sort = TRUE) %>%
      arrange(desc(n)) %>%
      ungroup()

    plot_words %>%
      ggplot(aes(word, 1, label = word, fill = sentiment)) +
      geom_point(color = "white") +
      geom_label_repel(force = 1, nudge_y = 0.5,
                       direction = "y",
                       box.padding = 0.04,
                       segment.color = "white",
                       size = 3) +
      facet_grid(~sentiment) +
      theme_lyrics() +
      theme(axis.text.y = element_blank(), axis.line.x = element_blank(),
            axis.title.x = element_blank(), axis.text.x = element_blank(),
            axis.ticks.x = element_blank(),
            panel.grid = element_blank(), panel.background = element_blank(),
            panel.border = element_rect("lightgray", fill = NA),
            strip.text.x = element_text(size = 9)) +
      xlab(NULL) + ylab(NULL) +
      ggtitle("Hotel California Words by NRC Sentiment") +
      coord_flip()

### seems to start off negatively, becomes positive in the middle, then dips back negative at end

    # we can chart this
    hcsentiment_index <- tidy_hc %>%
      inner_join(get_sentiments("nrc")%>%
                   filter(sentiment %in% c("positive",
                                           "negative"))) %>%
      count(index = line, sentiment) %>%
      spread(sentiment, n, fill = 0) %>%
      mutate(sentiment = positive - negative)

    hcsentiment_index %>%
      ggplot(aes(index, sentiment, fill = sentiment > 0)) +
      geom_col(show.legend = FALSE)
