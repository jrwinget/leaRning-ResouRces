Useful snippets
===============

Writing functions is like a receipe for baking a pie
----------------------------------------------------

    # skeleton; myfun is name of receipe, ingredidents go inside function(), steps go inside {}
    myfun <- function(){
      
    }

    # pie requires single ingredient
    myfun <- function(x){
      res <- x*2
      return(res)
    }

    mypie <- myfun(5)

    # sometimes might want to back 2 (or more) pies
    myfun <- function(x){
      res1 <- x*2
      res2 <- x/2
      return(list(res1 = res1,
                  res2 = res2))
    }

    mypie2 <- myfun(5)

    # can bake 2 pies with 2 ingredients each
    myfun <- function(x, f){
      res1 <- x * f
      res2 <- x / f
      return(list(res1 = res1,
                  res2 = res2))
    }

    pies <- myfun(5, 2)
    pies$res1
    pies$res2

    # some pies always use the same ingredient (e.g., vanilla extract)
    myfun <- function(x, f = 2){
      res <- x * f
      return(res)
    }

    mypie3 <- myfun(5)
    mypie3

    # make sure ingredients are fresh and suitable for the task at had, check each ingredient
    myfun <- function(x){
      if(!is.numeric(x))
        stop("x must be numeric, dummy!!")
      
      x <- x * 2
      
      return(x)
    }

    mypie4 <- myfun("5")

    # can define and call a function inside another function
    myfun <- function(x){
      
      check <- function(x){
        if(!is.numeric(x)) stop("Just stop trying...")
      }
      
      check(x)
    }

    mypie4 <- myfun("5")

    # can pass a "small" generic function as an argument to a "big" function
    myfun <- function(x, FUN){
      res <- FUN(x)
      return(res)
    }

    myfun(x = 5, FUN = floor) # then make the "small" function specific in the "big" function call
    myfun(x = 5, FUN = ceiling) # then make the "small" function specific in the "big" function call
