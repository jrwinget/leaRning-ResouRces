Writing a function
==================

Basics
------

    # function template
    my_fun <- function(arg1, arg2) {
      # body
    }

    # create function ratio that takes arguments x and y and returns their ratio
    ratio <- function(x, y) {
      x / y
    }

    ratio(3, 4)

    # subsetting lists: my_list[[1]] extracts the first element of the list my_list, and my_list[["name"]] extracts the element in my_list that is called name. If the list is nested you can travel down the hierarchy by recursive subsetting. For example, mylist[[1]][["name"]] is the element called name inside the first element of my_list. A data frame is just a special kind of list, so you can use double bracket subsetting on data frames too. my_df[[1]] will extract the first column of a data frame and my_df[["name"]] will extract the column named name from the data frame.

    # 2nd element in tricky_list
    typeof(tricky_list[[2]])

    # Element called x in tricky_list
    typeof(tricky_list[["x"]])

    # 2nd element inside the element called x in tricky_list
    typeof(tricky_list[["x"]][[2]])

    # names() will give names at the top level of the list and str() gives entrie desription of list

    # Guess where the regression model is stored
    names(tricky_list)

    # Use names() and str() on the model element
    names(tricky_list[["model"]])
    str(tricky_list[["model"]])

    # Subset the coefficients element
    tricky_list[["model"]][["coefficients"]]

    # Subset the wt element
    tricky_list[["model"]][["coefficients"]][["wt"]]

    # sequencing
    # common template (not the best way to generate such a sequence, espcailly if df if empty)
    i in 1:ncol(df) # each time the for loop iterates, i takes the next value in 1:ncol(df)

    # better way to sequence: seq_along()
    for (i in seq_along(df)) {
      print(median(df[[i]]))
    }

Keeping the output
------------------

    # before starting the loop, always allocate space for the output (eg, object called output). Very important for efficiency: growing the for loop at each iteration (e.g. using c()) will be very slow. A general way of creating an empty vector of given length is the vector() function. At each iteration of the loop, store the output in the corresponding entry of the output vector (i.e. assign the result to output[[i]] ). This subsetting will work whether output is a vector or a list.

    # Create new double vector: output
    output <- vector(ncol(df), mode = "double")

    # Alter the loop
    for (i in seq_along(df)) {
      # Change code to store result in output
      output[[i]] <- median(df[[i]])
    }

    # Print output
    print(output)

When/how to write a function
----------------------------

    # start with snippet of code that does what you want it to
    # rescale a column to be between 0 and 1
    (df$a - min(df$a, na.rm = TRUE)) /
      (max(df$a, na.rm = TRUE) - min(df$a, na.rm = TRUE))

    # Define example vector x
    x <- 1:10

    # Rewrite this snippet to refer to x
    (x - min(x, na.rm = TRUE)) /
      (max(x, na.rm = TRUE) - min(x, na.rm = TRUE))

    # rewrite for clarity: is there any duplication?
    # One obviously duplicated statement is min(x, na.rm = TRUE). It makes more sense for us just to calculate it once, store the result, and then refer to it when needed. In fact, since we also need the maximum value of x, it would be even better to calculate the range once, then refer to the first and second elements when they are needed.

    # Define rng
    rng <- range(x)

    # Rewrite this snippet to refer to the elements of rng
    (x - rng[[1]]) /
      (rng[[2]] - rng[[1]])

    # Use the function template to create the rescale01 function
    rescale01 <- function(x) {
      rng <- range(x, na.rm = TRUE)
      (x - rng[1]) / (rng[2] - rng[1])
    }

    # Test your function, call rescale01 using the vector x as the argument
    rescale01(x)

New problem: write a function, both\_na(), that counts how many positions 2 vectors both have a missing value
-------------------------------------------------------------------------------------------------------------

    # start by solving a simple problem first
    # Define example vectors x and y
    x <- c(1, 2, NA, 3, NA)
    y <- c(NA, 3, NA, 3, 4)

    # Count how many elements are missing in both x and y
    sum(is.na(x) & is.na(y)) # snippet of code that works

    # Turn snippet into a function: both_na()
    both_na <- function(x, y) {
      sum(is.na(x) & is.na(y))
    }

    # Define x, y1 and y2
    x <- c(NA, NA, NA)
    y1 <- c(1, NA, NA)
    y2 <- c(1, NA, NA, NA)

    # Call both_na on x, y1
    both_na(x, y1)

    # Call both_na on x, y2
    both_na(x, y2)

    # function names should be descriptive, but so do argument names

    # instead of this
    mean_ci <- function(c, nums) {
      se <- sd(nums) / sqrt(length(nums))
      alpha <- 1 - c
      mean(nums) + se * qnorm(c(alpha / 2, 1 - alpha / 2))
    }

    # do this
    mean_ci <- function(level, x) {
      se <- sd(x) / sqrt(length(x))
      alpha <- 1 - level
      mean(x) + se * qnorm(c(alpha / 2, level / 2))
    }

Data arguments should come before detail arguments
--------------------------------------------------

    # so instead of
    mean_ci <- function(level, x) {
      se <- sd(x) / sqrt(length(x))
      alpha <- 1 - level
      mean(x) + se * qnorm(c(alpha / 2, 1 - alpha / 2))
    }

    # do this
    mean_ci <- function(x, level = 0.95) {
      se <- sd(x) / sqrt(length(x))
      alpha <- 1 - level
      mean(x) + se * qnorm(c(alpha / 2, 1 - alpha / 2))
    }

Include warning labels if necessary. for example, mean\_ci returns ci with missing values when an empty vector is passed to it. sometimes an early return() is necessary
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    # so instead of
    mean_ci <- function(x, level = 0.95) {
      if (length(x) == 0) {
        warning("`x` was empty", call. = FALSE)
        interval <- c(-Inf, Inf)
      } else {
        se <- sd(x) / sqrt(length(x))
        alpha <- 1 - level
        interval <- mean(x) +
          se * qnorm(c(alpha / 2, 1 - alpha / 2))
      }
      interval
    }

    # do this
    mean_ci <- function(x, level = 0.95) {
      if (length(x) == 0) {
        warning("`x` was empty", call = FALSE)
        return(c(-Inf, Inf))
      } else {
        se <- sd(x) / sqrt(length(x))
        alpha <- 1 - level
        mean(x) + se * qnorm(c(alpha / 2, 1 - alpha / 2))
      }
    }

Putting it together
-------------------

    # poorly written function
    f <- function(x, y) {
      x[is.na(x)] <- y
      cat(sum(is.na(x)), y, "\n")
      x
    }

    # Rename the function f() to replace_missings()
    replace_missings <- function(x, replacement) {
      # Change the name of the y argument to replacement
      x[is.na(x)] <- replacement
      cat(sum(is.na(x)), replacement, "\n")
      x
    }

    # Replace missing values of df$z with 0s using new function
    df$z <- replace_missings(df$z, replacement = 0)

    # Reduce redundency
    replace_missings <- function(x, replacement) {
      # Define is_miss
      is_miss <- is.na(x)
      # Rewrite rest of function to refer to is_miss
      x[is_miss] <- replacement
      cat(sum(is_miss), replacement, "\n")
      x
    }

    # make sure the output is inutitive. right now, the output isn't exactly self-explanatory
    replace_missings(df$z, replacement = 0)

    # rewrite to use message and ouput sum(is_miss) missings replaced by the value replacement
    replace_missings <- function(x, replacement) {
      is_miss <- is.na(x)
      x[is_miss] <- replacement
      # Rewrite to use message()
      message(sum(is_miss), replacement, "\n")
      x
    }

    # Check your new function by running on df$z
    replace_missings(df$z, 0)

Functional programming (purrr package
-------------------------------------

    df <- data.frame(
      a = rnorm(10),
      b = rnorm(10),
      c = rnorm(10),
      d = rnorm(10)
    )

    # compute median of each column
    # Initialize output vector
    output <- vector("double", ncol(df))

    # Fill in the body of the for loop
    for (i in seq_along(df)) {
      output[[i]] <- median(df[[i]])
    }

    # View the result
    output

    # imagine needing to do this for multiple DFs; time for a function
    col_median <- function(df) {
      output <- vector("double", ncol(df))
      for (i in seq_along(df)) {
        output[[i]] <- median(df[[i]])
      }
      output
    }

    # write another one for means
    col_mean <- function(df) {
      output <- numeric(length(df))
      for (i in seq_along(df)) {
        output[[i]] <- mean(df[[i]])
      }
      output
    }

    # aaaaaaaand, one for SDs
    col_sd <- function(df) {
      output <- numeric(length(df))
      for (i in seq_along(df)) {
        output[[i]] <- sd(df[[i]])
      }
      output
    }

    # well, we've now copied and pasted col_median multiple times, so we should write a function. but we need one that will take column summary for any summary function
    # start with simpler example first
    f1 <- function(x) abs(x - mean(x))^1
    f2 <- function(x) abs(x - mean(x))^2
    f3 <- function(x) abs(x - mean(x))^3

    # write a function that takes arguments x and power
    f <- function(x, power) {
      # Edit the body to return absolute deviations raised to power
      abs(x - mean(x))^power
    }

    # we can also use functions as arguments, so to solve the column summary problem, we could write something like
    col_summary <- function(df, fun) {
      output <- vector("numeric", length(df))
      for (i in seq_along(df)) {
        output[[i]] <- fun(df[[i]])
      }
      output
    }

    # Find the column medians using col_median() and col_summary()
    col_median(df)
    col_summary(df, fun = median)


    # Find the column means using col_mean() and col_summary()
    col_mean(df)
    col_summary(df, fun = mean)

    # Find the column IQRs using col_summary()
    col_summary(df, fun = IQR)

    # an easier approach would be to use the map functions in purrr
    library(purrr)

    # Use map_dbl() to find column means
    map_dbl(df, mean)

    # Use map_dbl() to column medians
    map_dbl(df, median)

    # Use map_dbl() to find column standard deviations
    map_dbl(df, sd)

    # can also pass additional arguments to the functions that are called within map (ie, .f)
    map_dbl(df, mean, trim = 0.5)
    map_dbl(df, mean, trim = 0.5, na.rm = TRUE)
    map_dbl(planes, quantile, probs = .05, na.rm = TRUE)

map functions are type consistent, so you’ll know the class of output
---------------------------------------------------------------------

    # Find the columns that are numeric
    map_lgl(df3, is.numeric)

    # Find the type of each column
    map_chr(df3, typeof)

    # Find a summary of each column
    map(df3, summary)

Fit a separate linear regression of mpg against weight for each group of cars in our list of data frames, where each df in our list represents a different group
----------------------------------------------------------------------------------------------------------------------------------------------------------------

    # Examine the structure of cyl
    str(cyl)

    # Extract the first element into four_cyls
    four_cyls <- cyl[[1]]

    # Fit a linear regression of mpg on wt using four_cyls
    lm(mpg ~ wt, four_cyls) # snippet of code that preforms the operation we want

    # we could turn the snippet into a function then pass it into map, but it might be a bit much to define a function for a specific model only once.
    fit_reg <- function(df) {
      lm(mpg ~ wt, data = df)
    }

    map(cyl, fit_reg)

    # instead, we can use the function annonymously inside of the map() call
    map(cyl, function(df) lm(mpg ~ wt, data = df))

Writing anonymous functions uses a lot of extra key strokes, so purrr provides a shortcut
-----------------------------------------------------------------------------------------

    # for example, this
    map_dbl(cyl, function(df) mean(df$disp))

    # becomes this
    map_dbl(cyl, ~mean(.$disp))

    # so, our regression example would become
    map(cyl, ~lm(mpg ~ wt, data = .))

    # Save the result from the previous exercise to the variable models
    models <- map(cyl, ~lm(mpg ~ wt, data = .))

    # Use map and coef to get the coefficients for each model: coefs
    coefs <- map(models, coef)

    # Use string shortcut (rather than subsetting) to extract the wt coefficient
    map(coefs, "wt")

    # use map_dbl with the numeric shortcut to pull out the second element
    map_dbl(coefs, 2)

    # Define models
    models <- mtcars %>%
      split(mtcars$cyl) %>%
      map(~lm(mpg ~ wt, data = .))

    # Rewrite to be a single command using pipes
    models %>%
      map(summary) %>%
      map_dbl("r.squared")

Advanced inouts and outputs
---------------------------

    # safely() takes a function as an argument and it returns a function as its output. The function that is returned is modified so it never throws an error (and never stops the rest of your computation!).  it always returns a list with two elements: (1) result is the original result. If there was an error, this will be NULL. (2) error is an error object. If the operation was successful this will be NULL.

    # Create safe_readLines() by passing readLines() to safely()
    safe_readLines <- safely(readLines)

    # Call safe_readLines() on "http://example.org"
    safe_readLines("http://example.org")

    # Call safe_readLines() on "http://asdfasdasdkfjlda"
    safe_readLines("http://asdfasdasdkfjlda")

    # Use the safe_readLines() function with map(): html
    html <- map(urls, safe_readLines)

    # Call str() on html
    str(html)

    # Extract the result from one of the successful elements
    html[[1]][[1]]

    # Extract the error from the element that was unsuccessful
    html[[3]][[2]]
    ## Sometimes, the output isn't easy to work with. for example, in the previous extractions, the results/errors are buried in the inner-most level of the list. transpose() can turn a list of lists "inside-out" to make these outputs easier to work with

    # Examine the structure of transpose(html)
    str(transpose(html))

    # Extract the results: res
    res <- transpose(html)[["result"]]

    # Extract the errors: errs
    errs <- transpose(html)[["error"]]

Working errors and results
--------------------------

    # Initialize some objects
    safe_readLines <- safely(readLines)
    html <- map(urls, safe_readLines)
    res <- transpose(html)[["result"]]
    errs <- transpose(html)[["error"]]

    # Create a logical vector is_ok
    is_ok <- map_lgl(errs, is_null)

    # Extract the successful results
    res[is_ok]

    # Find the URLs that were unsuccessful
    urls[!is_ok]

Mapping over multiple arguments
-------------------------------

    # Create a list n containing the values: 5, 10, and 20
    n <- list(5, 10, 20)

    # Call map() on n with rnorm() to simulate three samples
    map(n, rnorm)

    # Now imagine we don't just want to vary the sample size, we also want to vary the mean. The mean can be specified in rnorm() by the argument mean. Now there are two arguments to rnorm() we want to vary: n and mean. The map2() function is designed exactly for this purpose; it allows iteration over two objects. The first two arguments to map2() are the objects to iterate over and the third argument .f is the function to apply.

    # Create a list mu containing the values: 1, 5, and 10
    mu <- list(1, 5, 10)

    # Edit to call map2() on n and mu with rnorm() to simulate three samples
    map2(n, mu, rnorm)

    # when there are more than 2 arguments, use pmap() - it takes a list of arguments as its input

    # Create a sd list with the values: 0.1, 1 and 0.1
    sd <- list(0.1, 1, 0.1)

    # Edit this call to pmap() to iterate over the sd list as well
    pmap(list(n, mu, sd), rnorm)

    # Name the elements of the argument list
    pmap(list(mean = mu, n = n, sd = sd), rnorm)

Mapping over their functions and their arguments
------------------------------------------------

    # Define list of functions
    f <- list("rnorm", "runif", "rexp")

    # Parameter list for rnorm()
    rnorm_params <- list(mean = 10)

    # Add a min element with value 0 and max element with value 5
    runif_params <- list(min = 0, max = 5)

    # Add a rate element with value 5
    rexp_params <- list(rate = 5)

    # Define params for each function
    params <- list(
      rnorm_params,
      runif_params,
      rexp_params
    )

    # Call invoke_map() on f supplying params as the second argument
    invoke_map(f, params, n = 5)

    # walk() operates just like map() but it's designed for functions that don't return anything (eg, printing, plotting, saving)

    # Assign the simulated samples to sims
    sims <- invoke_map(f, params, n = 50)

    # Use walk() to make a histogram of each element in sims
    walk(sims, hist)

    # we'd like to include better breaks for the bins in the histograms, so we need 2 arguments: x and breaks
    breaks_list <- list(
      Normal = seq(6, 16, 0.5),
      Uniform = seq(0, 5, 0.25),
      Exp = seq(0, 1.5, 0.1)
    )

    # Use walk2() to make histograms with the right breaks
    walk2(sims, breaks_list, hist)

A better idea would be to generate reasonable breaks based on the actual values in our simulated samples, so write a function find\_breaks()
--------------------------------------------------------------------------------------------------------------------------------------------

    # snippet of code
    rng <- range(sims[[1]], na.rm = TRUE)
    seq(rng[1], rng[2], length.out = 30)

    # turn into function
    find_breaks <- function(x) {
      rng <- range(x, na.rm = TRUE)
      seq(rng[1], rng[2], length.out = 30)
    }

    # Call find_breaks() on sims[[1]]
    find_breaks(sims[[1]])

    # Use map() to iterate find_breaks() over sims: nice_breaks
    nice_breaks <- map(sims, find_breaks)

    # Use nice_breaks as the second argument to walk2()
    walk2(sims, nice_breaks, hist)

    # plots have ugly labels and titles, so let's use pwalk to clean things up
    # Increase sample size to 1000
    sims <- invoke_map(f, params, n = 1000)

    # Compute nice_breaks (don't change this)
    nice_breaks <- map(sims, find_breaks)

    # Create a vector nice_titles
    nice_titles <- c("Normal(10, 1)", "Uniform(0, 5)", "Exp(5)")

    # Use pwalk() instead of walk2()
    pwalk(list(x = sims, breaks = nice_breaks, main = nice_titles), hist, xlab = "")

    # Pipe this along to map(), using summary() as .f
    sims %>%
      walk(hist) %>%
      map(summary)

Robust functions
----------------

    # errors are better than surprises
    # Define troublesome x and y
    x <- c(NA, NA, NA)
    y <- c(1, NA, NA, NA)

    both_na <- function(x, y) {
      # Add stopifnot() to check length of x and y
      stopifnot(length(x) == length(y))
      sum(is.na(x) & is.na(y))
    }

    # Call both_na() on x and y
    both_na(x, y)

    # but an informative error is better
    both_na <- function(x, y) {
      # Replace condition with logical
      if (TRUE) {
        # Replace "Error" with better message
        stop("x and y must have the same length", call. = FALSE)
      }
      sum(is.na(x) & is.na(y))
    }

    # Call both_na()
    both_na(x, y)
